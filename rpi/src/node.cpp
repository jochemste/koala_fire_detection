#include "node.h"

Node::Node(std::string ID, float x, float y): ID_(ID),
				      location_(new Coordinates(x, y)),
				      timestamp_(time(NULL)){

}

Node::Node(std::string ID, Coordinates location): ID_(ID),
					  location_(new Coordinates(location)){
  
}

Node::~Node(){
  delete location_;
}

std::string Node::getID() const{
  return ID_;
}

void Node::setActive(bool activate){
  active_=activate;
}

bool Node::nodeActive(){
  return active_;
}

void Node::setState(int state){
  state_=state;

  if(state_ > 255)
    state_ = 255;
  else if(state_ < rgb_offset_)
    state_ = rgb_offset_;
}

void Node::setTempState(int tState){
  temp_state_=tState;
}

void Node::setHumState(int hState){
  hum_state_=hState;
}

void Node::setTemperature(float temperature){
  temperature_=temperature;

  if(temperature < 0.0) temperature=0.0;
  else if(temperature > 30.0) temperature=30.0;

  temp_state_ = static_cast<unsigned int>(temp_slope_*temperature+rgb_offset_);

  if(temp_state_ > 255) temp_state_ = 255;
  if(temp_state_ < rgb_offset_) temp_state_ = rgb_offset_;
  
  setTempState(static_cast<int>(temp_state_));

  if(getHumState()){
    float state = ((static_cast<float>(temp_state_)/
    		    static_cast<float>(temp_state_+hum_state_))*temp_state_ +
    		   (static_cast<float>(hum_state_)/
    		    static_cast<float>(temp_state_+hum_state_))*hum_state_);

    setState(static_cast<int>(state));
    calculateInterval();
  } else {
    setState(getTempState());
  }

}

void Node::setHumidity(int humidity){
  humidity_=humidity;

  if(humidity < 20) humidity=20;
  else if(humidity > 80) humidity=80;

  hum_state_ = static_cast<int>(hum_slope_*(humidity-hum_offset_)+rgb_hum_offset_);

  if(hum_state_ > 255) hum_state_ = 255;
  if(hum_state_ < rgb_offset_) hum_state_ = rgb_offset_;
  
  setHumState(static_cast<int>(hum_state_));

  if(getTempState()){
    float state = ((static_cast<float>(temp_state_)/
    		    static_cast<float>(temp_state_+hum_state_))*temp_state_ +
    		   (static_cast<float>(hum_state_)/
    		    static_cast<float>(temp_state_+hum_state_))*hum_state_);

    setState(static_cast<int>(state));
    calculateInterval();
  } else{
    setState(getHumState());
  }

}

void Node::setCounter(int counter){
  counter_=counter;
}

int Node::getState(){
  return state_;
}

int Node::getTempState(){
  return temp_state_;
}

int Node::getHumState(){
  return hum_state_;
}

void Node::setLocation(Coordinates newLocation){
  *location_=newLocation;
}

void Node::setLocation(float x, float y){
  location_->setX(x);
  location_->setY(y);
}

void Node::setTimeStamp(time_t currentTime){
  prev_time_=timestamp_;
  timestamp_=currentTime;
}

time_t Node::getTimeStamp(){
  return timestamp_;
}

double Node::getInterval(){
  return interval_;
}

double Node::getPreviousTime(){
  return prev_time_;
}

Coordinates Node::getLocation(){
  return *location_;
}

float Node::getLocationX(){
  return location_->getX();
}

float Node::getLocationY(){
  return location_->getY();
}

void Node::calculateInterval(){
  float ftemp = static_cast<float>(temperature_);
  float fhum = static_cast<float>(humidity_);

  if (ftemp < 0) ftemp = 0.0;
  else if (ftemp > 30) ftemp = 30.0;

  if (fhum < hum_offset_) fhum = hum_offset_;
  else if (fhum > 80) fhum = 80.0;

  float interval_temp = (-0.7997222*ftemp+24.0)*3600.0;
  float interval_hum = (0.398527*(fhum-hum_offset_))*3600.0+30.0;

  float weight_temp = 1.0-(interval_temp/(interval_temp+interval_hum));
  float weight_hum = 1.0-(interval_hum/(interval_temp+interval_hum));

  interval_ = static_cast<int>(weight_temp*interval_temp + weight_hum*interval_hum);
}
