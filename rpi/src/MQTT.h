#ifndef MQTT_H
#define MQTT_H

#include "ParQueue.h"
#include "SenseHAT_Koala.h"

#include <json.hpp>
#include <mosquittopp.h>
#include <string>
#include <mutex>
#include <vector>

using json = nlohmann::json;

/*! 
 *  \brief     Class to deal with MQTT
 *  \details   A MQTT client class, publicly derived from mosqpp::mosquittopp.
 *  \author    Jochem Stevense
 *  \version   1.0
 *  \date      2020
 *  \copyright GNU Public License.
 */
class MQTT : public mosqpp::mosquittopp
{
public:
  using topic_message_t = std::pair<std::string, json>;

  /*!
   * \brief     Constructor
   * \details   Constructor
   * \param     appname
   * \param     clientname
   * \param     host
   * \param     port
   */
  MQTT(const std::string &appname, const std::string &clientname,
       const std::string &host, int port);

  MQTT(const MQTT &other) = delete; /// Copying not allowed

  MQTT &operator=(const MQTT &other) = delete; /// Copying not allowed

  virtual ~MQTT(); /// Virtual destructor

  /*!
   * \brief     Initialise pointers to allow messaging.
   * \details   Initialises pointers to mutex, message buffer and boolean
   *            to allow the main thread to receive message in a 
   *            thread safe manner.
   * \param     mut The address of the mutex used in main thread.
   * \param     mess The address of the buffer used in main thread.
   * \param     rec The address of the boolean used in main thread to 
   *                indicate a received message.
   */
  void init_messages(std::mutex& mut,
		     std::vector<topic_message_t>& mess,
		     bool& rec);
  
protected:
  const std::string className_; /// Member variable to store the class name
  const std::string mqttID_; /// ID of the connection

  /*!
   * \brief     Function that gets called when the program connected.
   * \details   Function that gets called when the program connected.
   * \param     rc The connection code. 0 when no error ocurred.
   */
  void on_connect(int rc) override;

  /*!
   * \brief     Function that gets called when the program disconnects.
   * \details   Function that gets called when the program disconnects.
   * \param     The connection code. 0 when no error ocurred.
   */
  void on_disconnect(int rc) override;

  /*!
   * \brief     Function that gets called when a message is posted on 
   *            the topic.
   * \details   Function that gets called when a message is posted on 
   *            the topic. Sets a boolean, so \a handle() can
   *            process the message.
   * \param     message A pointer to the received message.
   */
  void on_message(const struct mosquitto_message *message) override;

  /*!
   * \brief     Function that gets called when the program subscribed to 
   *            the right topic(s).
   * \details   Function that gets called when the program subscribed to 
   *            the right topic(s).
   * \param     mid
   * \param     qos_count
   * \param     granted_qos
   */
  void on_subscribe(int mid, int qos_count, const int *granted_qos) override;
  
  /*!
   * \brief     Function that gets called to log events and print them 
   *            on screen.
   * \details   Function that gets called to log events and print them 
   *            on screen.
   * \param     level The level of the log.
   * \param     str Pointer to the string to be printed.
   */
  void on_log(int level, const char *str) override;

  /*!
   * \brief     Function that gets called if an error occurs
   * \details   Function that gets called if an error occurs
   */
  void on_error() override;
  
private:
  ParQueue<topic_message_t> queue_; /// Currently unused
  std::mutex* message_mutex_ptr_; /// Mutex pointer, to point to the same mutex as in the main thread
  std::vector<topic_message_t>* messages_ptr_; /// Pointer to vector, used in the main thread, to be used as message buffer
  bool* message_received_ptr_; /// Pointer to boolean, used in the main thread, to indicate a received message
  bool message_transmit_=false; /// Boolean to make sure only actual messages are added to the buffer

  /*!
   * \brief     Private function to handle incoming messages
   * \details   The function handles incoming messages, locks the mutex 
   *            and adds the messages to the buffer, after which it 
   *            unlocks the mutex.
   * \param     topic_message The json format message to be added to the 
   *            buffer.
   */  
  void handle(const topic_message_t& topic_message);
};

#endif
