#include "SenseHAT_Koala.h"
#include "LedMatrix.h"

const Pixel SenseHAT_Koala::background_{Pixel{0, 50, 0}};
const Pixel SenseHAT_Koala::letters_{Pixel{255, 0, 255}};

SenseHAT_Koala::SenseHAT_Koala()
   : SenseHAT{}
{
   leds.clear(background_);
}

void SenseHAT_Koala::drawTest(){
  Pixel p(background_);
  leds.clear();
  for(int column=0; column<columns_; column++){
    for(int row=0; row<rows_; row++){
      leds.setPixel(row, column, p);
      std::this_thread::sleep_for(std::chrono::nanoseconds(30000000));
    }
  }
  leds.clear(background_);
}


void SenseHAT_Koala::drawEnd(){
  Pixel p(0, 0, 0);
  leds.clear(background_);
  for(int column=columns_-1; column>=0; column--){
    for(int row=rows_-1; row>=0; row--){
      leds.setPixel(row, column, p);
      std::this_thread::sleep_for(std::chrono::nanoseconds(30000000));
    }
  }
  leds.clear(background_);
}


void SenseHAT_Koala::draw(int column, int row, int r, int g, int b){
  leds.setPixel(row, column, Pixel(r, g, b));
}

void SenseHAT_Koala::draw(int column, int row, Pixel& pixel){
  leds.setPixel(column, row, pixel);
}

void SenseHAT_Koala::drawR(int duration){
  Pixel p=letters_;
  time_t timer=time(NULL);
  int startColumn=3;
  leds.clear(background_);

  for(int row=0; row<rows_; row++)
    draw(startColumn, row, p);

  draw(startColumn+1, 0, p);
  draw(startColumn+1, 3, p);
  
  draw(startColumn+2, 1, p);
  draw(startColumn+2, 2, p);
  int column=startColumn;
  for(int row=3; row<rows_; row++)
    draw(column++, row, p);

  while(duration>(time(NULL)-timer));
}

void SenseHAT_Koala::drawT(int duration){
  Pixel p=letters_;
  time_t timer=time(NULL);
  leds.clear(background_);
  int startColumn=3;

  for(int row=0; row<rows_; row++)
    draw(startColumn, row, p);

  for(int column=startColumn-2; column<startColumn+3; column++)
    draw(column, 0, p);

  while(duration>(time(NULL)-timer));
}

void SenseHAT_Koala::drawH(int duration){
  Pixel p=letters_;
  time_t timer=time(NULL);
  leds.clear(background_);
  int startColumn=3;
  
  for(int row=0; row<rows_; row++)
    draw(startColumn-1, row, p);

  for(int row=0; row<rows_; row++)
    draw(startColumn+1, row, p);

  for(int column=startColumn-1; column<=startColumn+1; column++)
    draw(column, rows_/2-1, p);

  while(duration>(time(NULL)-timer));
}

void SenseHAT_Koala::toggle(int column, int row, int r, int g, int b){
  Pixel pixel(r, g, b);
  if(leds.getPixel(column, row) == pixel){
    leds.setPixel(column, row, background_);
  } else {
    leds.setPixel(column, row, pixel);
  }
}

void SenseHAT_Koala::toggle(int column, int row, Pixel& pixel){
  if(leds.getPixel(column, row) == pixel){
    leds.setPixel(column, row, background_);
  } else {
    leds.setPixel(column, row, pixel);
  }
}

void SenseHAT_Koala::clear(int r, int g, int b){
  leds.clear(Pixel(r, g, b));
}

void SenseHAT_Koala::clear(Pixel& pixel){
  leds.clear(pixel);
}

void SenseHAT_Koala::clear(){
  leds.clear();
}

//////////////PRIVATE////////////////////////
