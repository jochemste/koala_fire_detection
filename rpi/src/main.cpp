#include "AppInfo.h"
#include "SH_ui.h"
#include "TTNconfig.h"
#include "MQTT.h"
#include "Koala_main.h"

#include <atomic>
#include <csignal>
#include <iostream>
#include <unistd.h>

using namespace std;

volatile sig_atomic_t receivedSIGINT{false};

void handleSIGINT(int /* s */)
{
   receivedSIGINT = true;
}

int main(int argc, char *argv[])
{
  cout << "\n*\n------  Program started  ------\n*\n";

  Koala_main koala_app;
  /*
  int majorMosquitto{0};
  int minorMosquitto{0};
  int revisionMosquitto{0};
  SH_ui user_interface;
  
  string mqttBroker{TTN_MQTT_HOST};
  int mqttBrokerPort{TTN_MQTT_PORT};
  */
  
  switch (argc) {
  case 1:
    break;
  default:
    cerr << "\nERROR no command line arguments allowed\n\n";
    exit(EXIT_FAILURE);
  }
  
  /*
  cout << "-- MQTT application: " << APPNAME_VERSION << "  ";
  mosqpp::lib_init();
  mosqpp::lib_version(&majorMosquitto, &minorMosquitto, &revisionMosquitto);
  cout << "uses Mosquitto lib version " << majorMosquitto << '.'
       << minorMosquitto << '.' << revisionMosquitto << endl;
  */
  
  try {
    signal(SIGINT, handleSIGINT);
    koala_app.init();
    /*
    MQTT mqtt("MQTT", "mqtt", mqttBroker, mqttBrokerPort);
    
    // Checking rc for reconnection, 'clients' is an initializer_list
    auto clients = {static_cast<mosqpp::mosquittopp *>(&mqtt)};
    
    cout << "-- LoRaWAN for MQTT The Things Network is ready" << endl;
    */

    /*
    //TESTING//////////////////////////////////////////////////////////////
    std::cout << "*\n*\n*\n";    

    user_interface.add_node("112", 5.2, 10.0);
    user_interface.set_node_state_risk(user_interface.get_node_index("112"), 1);
    user_interface.add_node("911", 10.6, 5.3);
    user_interface.set_node_state_risk(user_interface.get_node_index("911"), 2);
    user_interface.add_node("999", 20.7, 20.3);
    user_interface.set_node_state_risk(user_interface.get_node_index("999"), 4);

    for(int i=0; i<user_interface.get_nr_nodes(); i++){
      cout << "Location of node " << i << " with ID " << user_interface.get_node_ID(i)
	   <<": " << user_interface.get_node_x(i)
	   << ", " << user_interface.get_node_y(i) << endl;
    }

    user_interface.draw_nodes_risk();
    //user_interface.print_risk_matrix();
    std::cout << "\n\n";
    user_interface.print_risk_matrix_SH();

    std::cout << "\n\n";
    user_interface.add_node("632", 100.2, 23.1);
    user_interface.set_node_state_risk(user_interface.get_node_index("632"), 6);

    user_interface.draw_nodes_risk();
    //user_interface.print_risk_matrix();
    std::cout << "\n\n";
    user_interface.print_risk_matrix_SH();

    std::cout << "\n\n";
    user_interface.add_node("684356", 100.2, 50.9);
    user_interface.set_node_state_risk(user_interface.get_node_index("684356"), 5);

    user_interface.draw_nodes_risk();
    //user_interface.print_risk_matrix();
    std::cout << "\n\n";
    user_interface.print_risk_matrix_SH();

    std::cout << std::endl << user_interface.get_node_index("911") << std::endl;
    std::cout << user_interface.get_node_index("684356") << std::endl;
    
    std::cout << "*\n*\n*\n";    
    //END TESTING//////////////////////////////////////////////////////////////
    */
    
    while (!receivedSIGINT) {
      koala_app.run();
      /*
      // Error checking
      for (auto client : clients) {
	int rc = client->loop();
	if (rc) {
	  cerr << "-- MQTT reconnect" << endl;
	  client->reconnect();
	}
      }

      //Main loop
      */
    }
  }
  catch (exception &e) {
    cerr << "Exception " << e.what() << endl;
  }
  catch (...) {
    cerr << "UNKNOWN EXCEPTION\n";
  }

  /*
  cout << "-- MQTT application: " << APPNAME_VERSION << " stopped" << endl
       << endl;
  
  mosqpp::lib_cleanup();
  */
  
  return 0;
}
