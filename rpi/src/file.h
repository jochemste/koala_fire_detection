#ifndef FILE_H
#define FILE_H

#include <fstream>
#include <vector>
#include <iostream>
#include <string>

/*! 
 *  \brief     Class to handle writing and reading a files contents.
 *  \details   This class is used to write into a file and to read contents of
 *             a file without having to worry about opening and closing it safely.
 *  \author    Jochem Stevense
 *  \version   1.0
 *  \date      2020
 *  \pre       First use \a set_fd() to set the file descriptor.
 *  \copyright GNU Public License.
 */
class File{
 public:

  /*!
   * \brief     Constructor
   * \details   Empty constructor. Currently does nothing.
   */
  File();

  /*!
   * \brief     Destructor
   * \details   Empty destructor. Currently does nothing.
   */
  ~File();

  /*!
   * \brief     Sets the file descriptor.
   * \details   Sets the file descriptor.
   * \param     fd The path/filename to be used for the file descriptor.
   */
  void set_fd(std::string fd);

  /*!
   * \brief     Append a string to the file.
   * \details   Appends a string to the end of the file.
   * \param     data The string to be appended to the file.
   */
  void append(std::string data);

  /*!
   * \brief     Erases the contents of a file
   * \details   Erases the contents of a file
   */
  void erase_all();

  /*!
   * \brief     Read the contents of a file and return it as an integer.
   * \details   Read the contents of a file and return it as an 
   *            integer. Used for files with nothing but a single
   *            integer in them.
   * \returns   The contents of a file as an integer.
   */
  int read_int();
  
 private:
  std::ofstream outfile_; /// File stream to write in.
  std::string fd_=""; /// File name/path.
};

#endif
