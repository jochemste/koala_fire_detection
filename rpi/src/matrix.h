#ifndef MATRIX_H
#define MATRIX_H

#include <iostream>
#include <vector>

/*! 
 *  \brief     A class to hold the state in a flexible matrix
 *  \details   This class is used to handle the main processes of the program. 
 *  \author    Jochem Stevense
 *  \version   1.0
 *  \date      2020
 *  \copyright GNU Public License.
 */
class Matrix{
  
 public:
  
  /*!
   * \brief     Constructor
   * \details   Currently does nothing
   */
  Matrix();
  
  /*!
   * \brief     Destructor
   * \details   Currently does nothing
   */
  ~Matrix();

  /*!
   * \brief     Initialise a matrix.
   * \details   Initialise a matrix, using the minimum and maximum
   *            values and the size as parameters.
   * \param     minX The minimum X value
   * \param     maxX The maximum X value
   * \param     minY The minimum Y value
   * \param     maxY The maximum Y value
   * \param     size The desired size of the matrix
   */
  void initMatrix(float minX, float maxX, float minY,
		  float maxY, int size);

  /*!
   * \brief     Function to see if a coordinate would fit into the matrix
   * \details   Function to see if a coordinate would fit into the matrix.
   * \param     x X coordinate
   * \param     y Y coordinate
   * \returns   A boolean indicating if the coordinates fit or not
   */
  bool fitsInMatrix(float x, float y);

  /*!
   * \brief     Function to get the column a X coordinate fits into
   * \details   Function to get the column a X coordinate fits into. 
   *            Returns -1 if X does not fit into column
   * \param     x X coordinate
   * \returns   The number of the column \a x fits into or -1 if it 
   *            does not fit.
   */
  int fitsInColumn(float x);

  /*!
   * \brief     Function to get the row a Y coordinate fits into
   * \details   Function to get the row a X coordinate fits into. 
   *            Returns -1 if Y does not fit into row
   * \param     y Y coordinate
   * \returns   The number of the row \a y fits into or -1 if it does not fit.
   */
  int fitsInRow(float y);

  /*!
   * \brief     Adds a grid to the matrix
   * \details   Adds a row and column to the matrix
   */
  void addGrid();

  /*!
   * \brief     Adds a nr of grids to the matrix
   * \details   Adds an equal nr of rows and columns to the matrix
   * \param     nr The number of grids to add
   */
  void addGrid(int nr);

  /*!
   * \brief     Adds a row to all the columns in the matrix
   * \details   Adds a row to all the columns in the matrix
   */
  void addRow();
  
  /*!
   * \brief     Adds a nr of rows to all the columns in the matrix
   * \details   Adds a nr of rows to all the columns in the matrix
   * \param     nr Number of rows to add
   */
  void addRow(int nr);
  
  /*!
   * \brief     Adds a column to the matrix
   * \details   Adds a column to the matrix
   */
  void addColumn();
  
  /*!
   * \brief     Adds a nr of columns to the matrix
   * \details   Adds a nr of columns to the matrix
   * \param     nr Number of columns to add
   */
  void addColumn(int nr);

  /*!
   * \brief     Gets the number of columns in the matrix
   * \details   Gets the number of columns in the matrix
   * \returns   The number of columns is returned as an integer 
   */
  int getNrColumns();

  /*!
   * \brief     Gets the number of rows in the matrix
   * \details   Gets the number of rows in the matrix
   * \returns   The number of rows is returned as an integer 
   */
  int getNrRows();

  /*!
   * \brief     Gets the current state of an element
   * \details   Gets the current state of an element
   * \param     column The column to search in
   * \param     row The row to search in
   * \returns   The state of the element in the specified \a 
   *            column and \a row 
   */
  int getElementState(int column, int row);

  /*!
   * \brief     Sets the state of an element
   * \details   Sets the state of an element
   * \param     state The state the element should take
   * \param     column The column to search in
   * \param     row The row to search in
   */
  void setElementState(int state, int column, int row);

  /*!
   * \brief     Gets the number of grids in the matrix
   * \details   Gets the number of rows and columns in the matrix. 
   *            Returns -1 if the number of rows is not equal to the
   *            number of columns.
   * \returns   The number of grids in the matrix, or -1 if the nr of 
   *            rows and columns is not equal.
   */
  int getSize();

  /*!
   * \brief     Clears the matrix
   * \details   Clears all elements in the matrix
   */
  void clear();

  /*!
   * \brief     Transforms matrix to desired size.
   * \details   Adds grids or cuts out empty grids if possible to create
   *            a matrix of the desired size. Will return the matrix
   *            without warnings if it is bigger than desired.
   * \param     size The desired size of the matrix
   * \returns   A resized \a Matrix object
   */
  Matrix transformToMatrix(int size);

  /*!
   * \brief     Prints the matrix in its raw size
   * \details   Prints the matrix in its true size
   */
  void printMatrix();

  /*!
   * \brief     Prints the matrix in its specified size
   * \details   Prints the matrix in its specified size
   * \param     size The desired size of the matrix
   */
  void printTransformedMatrix(int size);

 private:
  std::vector<std::vector<int>> elements_={}; //!< A vector of vectors to hold all states in a matrix

  std::vector<float> columnCoords_={}; //!< A vector to hold the X/longitude coordinates for easy division into columns
  std::vector<float> rowCoords_={}; //!< A vector to hold the Y/latitude coords for easy division into rows

  int nrRows_=0; //!< The number of rows in the matrix
  int nrColumns_=0; //!< The number of columns in the matrix

  /*!
   * \brief     Initialises column coordinates (\a columnCoords_)
   * \details   Initialises column coordinates (\a columnCoords_)
   * \param     minX Smallest X coordinate
   * \param     maxX Biggest X coordinate
   */
  void initColCoords(float minX, float maxX);

  /*!
   * \brief     Initialises row coordinates (\a rowCoords_)
   * \details   Initialises row coordinates (\a rowCoords_)
   * \param     minY Smallest Y coordinate
   * \param     maxY Biggest Y coordinate
   */
  void initRowCoords(float minY, float maxY);

  /*!
   * \brief     Erases empty rows and columns if possible
   * \details   Erases an equal number of rows and grids to allow the
   *            matrix to match the \a desiredSize.
   * \param     desiredSize The desired size to shrink the matrix to
   */
  void eraseEmptyGrids(int desiredSize);
};

#endif
