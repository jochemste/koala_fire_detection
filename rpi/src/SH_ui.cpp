#include "SH_ui.h"

const int SH_ui::SH_SIZE=8;
const int SH_ui::MAX_PIX=255;
const int SH_ui::MIN_PIX=1;

SH_ui::SH_ui(){
  sh_koala_->drawTest();
}

SH_ui::~SH_ui(){
  sh_koala_->drawEnd();
  sh_koala_->clear();
  delete sh_koala_;
  delete matrixRisk_, matrixHum_, matrixTemp_;
}


void SH_ui::add_node(std::string ID, float x, float y){
  Node* newNode=new Node(ID, x, y);
  nodes_.push_back(newNode);
}



void SH_ui::add_node(std::string ID, Coordinates& coord){
  Node* newNode=new Node(ID, coord);
  nodes_.push_back(newNode);
}



Coordinates SH_ui::get_node_location(int index){
  return nodes_[index]->getLocation();
}


float SH_ui::get_node_x(int index){
  return nodes_[index]->getLocationX();
}

float SH_ui::get_node_y(int index){
  return nodes_[index]->getLocationY();
}


int SH_ui::get_node_index(Coordinates& location){
  int index=0;

  for(auto& el: nodes_){
    if (el->getLocation() == location){
      return index; 
    }
    index++;
  }
  
  return -1;
}

int SH_ui::get_node_index(float x, float y){
  int index=0;
  Coordinates location(x, y);
  
  for(auto& el: nodes_){
    if (el->getLocation() == location){
      return index; 
    }
    index++;
  }
  
  return -1;
}

int SH_ui::get_node_index(std::string ID){
  int index=0;
  
  for(auto& el: nodes_){
    if (el->getID() == ID){
      return index; 
    }
    index++;
  }
  
  return -1;
}

int SH_ui::get_nr_nodes(){
  return nodes_.size();
}

int SH_ui::get_node_state(int index){
  return nodes_[index]->getState();
}

std::string SH_ui::get_node_ID(int index){
  return nodes_[index]->getID();
}

void SH_ui::get_node_timedata(int index, int& interval, time_t& timestamp){
  interval=nodes_[index]->getInterval();
  timestamp=nodes_[index]->getTimeStamp();
}

void SH_ui::set_node_message(int index, float temperature, int humidity, int counter){
  set_node_temperature(index, temperature);
  set_node_humidity(index, humidity);
  set_node_counter(index, counter);
  nodes_[index]->setTimeStamp();
}

void SH_ui::set_node_state_risk(int index, int state){
  nodes_[index]->setState(state);
}

void SH_ui::set_node_state_temp(int index, int state){
  nodes_[index]->setTempState(state);
}

void SH_ui::set_node_state_hum(int index, int state){
  nodes_[index]->setHumState(state);
}

void SH_ui::set_node_temperature(int index, float temperature){
  nodes_[index]->setTemperature(temperature);
}

void SH_ui::set_node_humidity(int index, int humidity){
  nodes_[index]->setHumidity(humidity);
}

void SH_ui::set_node_counter(int index, int counter){
  nodes_[index]->setCounter(counter);
}

void SH_ui::draw_nodes_risk(){
  int gridsInMatrix=1;
  bool setMatrix=true;

  while(setMatrix){
    setMatrix=false;
    matrixRisk_->clear();
    matrixRisk_->initMatrix(getMinX(), getMaxX(),
			    getMinY(), getMaxY(), gridsInMatrix);
    std::cout << "Creating risk matrix for " << nodes_.size() << " nodes.\n";
    for(auto& node: nodes_){
      if(matrixRisk_->fitsInMatrix(node->getLocationX(), node->getLocationY())){
	 int column=matrixRisk_->fitsInColumn(node->getLocationX());
	 int row=matrixRisk_->fitsInColumn(node->getLocationY());
	 int state=node->getState();
	 std::cout << "Node state: " << state << std::endl;

	 
	 if(matrixRisk_->getElementState(column, row)==0){
	   
	   matrixRisk_->setElementState(state, column, row);
	 } else {
	   setMatrix=true;
	 }
      }
    }
    gridsInMatrix++;
  }

  sh_koala_->drawR(5);
  
  sh_koala_->clear();
  std::cout << "->Putting matrix on screen\n";
  Matrix sh_matrix=matrixRisk_->transformToMatrix(SH_SIZE);
  std::cout << "Transformed matrix created\n";
  for(int column=0; column<sh_matrix.getSize(); column++){
    for(int row=0; row<sh_matrix.getSize(); row++){
      Pixel p = state_to_pixel(sh_matrix.getElementState(column, row));
      sh_koala_->draw(column, row, p);
    }
  }
}

void SH_ui::draw_nodes_temp(){
  int gridsInMatrix=1;
  bool setMatrix=true;

  while(setMatrix){
    setMatrix=false;
    matrixTemp_->clear();
    matrixTemp_->initMatrix(getMinX(), getMaxX(),
			    getMinY(), getMaxY(), gridsInMatrix);
    
    std::cout << "Creating temperature matrix for " << nodes_.size() << " nodes.\n";
    for(auto& node: nodes_){
      if(matrixTemp_->fitsInMatrix(node->getLocationX(), node->getLocationY())){
	 int column=matrixTemp_->fitsInColumn(node->getLocationX());
	 int row=matrixTemp_->fitsInColumn(node->getLocationY());
	 int state=node->getTempState();
	 std::cout << "Node state: " << state << std::endl;
	 
	 if(matrixTemp_->getElementState(column, row)==0){
	   matrixTemp_->setElementState(state, column, row);
	 } else {
	   setMatrix=true;
	 }
      }
    }
    gridsInMatrix++;
  }

  sh_koala_->drawT(5);

  sh_koala_->clear();
  std::cout << "->Putting matrix on screen\n";
  Matrix sh_matrix=matrixTemp_->transformToMatrix(SH_SIZE);
  std::cout << "Transformed matrix created\n";
  for(int column=0; column<sh_matrix.getSize(); column++){
    //std::cout << "Parsing column " << column << std::endl;
    for(int row=0; row<sh_matrix.getSize(); row++){
      //std::cout << "Parsing row " << row << std::endl;
      Pixel p = stateT_to_pixel(sh_matrix.getElementState(column, row));
      sh_koala_->draw(column, row, p);
    }
  }
}

void SH_ui::draw_nodes_hum(){
  int gridsInMatrix=1;
  bool setMatrix=true;

  while(setMatrix){
    setMatrix=false;
    matrixHum_->clear();
    matrixHum_->initMatrix(getMinX(), getMaxX(),
			    getMinY(), getMaxY(), gridsInMatrix);

    std::cout << "Creating humidity matrix for " << nodes_.size() << " nodes.\n";
    for(auto& node: nodes_){
      if(matrixHum_->fitsInMatrix(node->getLocationX(), node->getLocationY())){
	 int column=matrixHum_->fitsInColumn(node->getLocationX());
	 int row=matrixHum_->fitsInColumn(node->getLocationY());
	 int state=node->getHumState();
	 std::cout << "Node state: " << state << std::endl;
	 
	 if(matrixHum_->getElementState(column, row)==0){
	   
	   matrixHum_->setElementState(state, column, row);
	 } else {
	   setMatrix=true;
	 }
      }
    }
    gridsInMatrix++;
  }

  sh_koala_->drawH(5);

  sh_koala_->clear();
  std::cout << "->Putting matrix on screen\n";
  Matrix sh_matrix=matrixHum_->transformToMatrix(SH_SIZE);
  std::cout << "Transformed matrix created\n";
  for(int column=0; column<sh_matrix.getSize(); column++){
    //std::cout << "Parsing column " << column << std::endl;
    for(int row=0; row<sh_matrix.getSize(); row++){
      //std::cout << "Parsing row " << row << std::endl;
      Pixel p = stateH_to_pixel(sh_matrix.getElementState(column, row));
      sh_koala_->draw(column, row, p);
    }
  }
}

void SH_ui::print_risk_matrix(){
  matrixRisk_->printMatrix();
}

void SH_ui::print_temp_matrix(){
  matrixTemp_->printMatrix();
}

void SH_ui::print_hum_matrix(){
  matrixHum_->printMatrix();
}

void SH_ui::print_risk_matrix_SH(){
  matrixRisk_->printTransformedMatrix(SH_SIZE);
}

void SH_ui::print_temp_matrix_SH(){
  matrixTemp_->printTransformedMatrix(SH_SIZE);
}

void SH_ui::print_hum_matrix_SH(){
  matrixHum_->printTransformedMatrix(SH_SIZE);
}

//////////////PRIVATE////////////////////////

Pixel SH_ui::state_to_pixel(int state){
  if(state==0)
    return Pixel(0, 0, 0);

  int red=state;
  int green=MAX_PIX-red;
  int blue=0;

  ensure_rgb_withing_bounds(red, green, blue);

  std::cout << "RISK-> Red: " << red << " Green: " << green << " Blue: " << blue << std::endl;
  
  Pixel p(red, green, blue);
  return p;
}

Pixel SH_ui::stateT_to_pixel(int tstate){
  if(tstate==0)
    return Pixel(0, 0, 0);


  int red = tstate;
  int green = MAX_PIX-red;
  int blue = 0;
  
  ensure_rgb_withing_bounds(red, green, blue);

  std::cout << "TEMP-> Red: " << red << " Green: " << green << " Blue: " << blue << std::endl;
  
  Pixel p(red, green, blue);
  return p;
}

Pixel SH_ui::stateH_to_pixel(int hstate){
  if(hstate==0)
    return Pixel(0, 0, 0);

  int red = hstate;
  int blue=MAX_PIX-red;
  int green=red;

  ensure_rgb_withing_bounds(red, green, blue);

  std::cout << "HUM-> Red: " << red << " Green: " << green << " Blue: " << blue << std::endl;

  Pixel p(red, green, blue);
  return p;
}

void SH_ui::ensure_rgb_withing_bounds(int& red, int& green, int& blue){
  if(red>MAX_PIX)
    red=MAX_PIX;
  if(green>MAX_PIX)
    green=MAX_PIX;
  if(blue>MAX_PIX)
    blue=MAX_PIX;
  
  if(red<MIN_PIX)
    red=MIN_PIX;
  if(green<MIN_PIX)
    green=MIN_PIX;
  if(blue<MIN_PIX)
    blue=MIN_PIX;
}

float SH_ui::getMaxX(){
  float maxX=0.0;
  for(auto& node: nodes_){
    float x=node->getLocationX();
    if(x>maxX)
      maxX=x;
  }
  return maxX;
}

float SH_ui::getMaxY(){
  float maxY=0.0;
  for(auto& node: nodes_){
    float y=node->getLocationY();
    if(y>maxY)
      maxY=y;
  }
  return maxY;
}

float SH_ui::getMinX(){
  float minX=nodes_[0]->getLocationX();
  for(auto& node: nodes_){
    float x=node->getLocationX();
    if(x<minX)
      minX=x;
  }
  return minX;
}

float SH_ui::getMinY(){
  float minY=nodes_[0]->getLocationY();
  for(auto& node: nodes_){
    float y=node->getLocationY();
    if(y<minY)
      minY=y;
  }
  return minY;
}
