#include "Koala_main.h"

auto clients_=0;

Koala_main::Koala_main(): className_(__func__){
  File buildfile;
  std::stringstream fd;
  
  buildfile.set_fd(buildnr_file_);
  int nr_builds=buildfile.read_int();

  fd << directory_ << file_name_ << nr_builds << extension_;
  full_file_name_=fd.str();
  file_->set_fd(full_file_name_);

  std::cout << APPNAME_VERSION << " started" << std::endl;
  std::cout << "Build number: " << nr_builds << std::endl;
}

Koala_main::~Koala_main(){
  std::cout << std::endl << APPNAME_VERSION << " ended" << std::endl;
  mosqpp::lib_cleanup();

  delete ui_, file_, mqtt_;
}

void Koala_main::init(){
  std::cout << "Initialising " << className_ << std::endl;
  int majorMosquitto{0};
  int minorMosquitto{0};
  int revisionMosquitto{0};
  
  std::string mqttBroker{TTN_MQTT_HOST};
  int mqttBrokerPort{TTN_MQTT_PORT};

  mosqpp::lib_init();
  mosqpp::lib_version(&majorMosquitto, &minorMosquitto, &revisionMosquitto);

  mqtt_= new MQTT("MQTT", "mqtt", mqttBroker, mqttBrokerPort);
  //Enable messaging between threads
  mqtt_->init_messages(message_mutex_,
		       messages_buffer_, message_received_);

  clients_ = {static_cast<mosqpp::mosquittopp *>(mqtt_)};
  std::cout << "-- LoRaWAN for MQTT The Things Network is ready" << std::endl;
  upd_timer_=time(NULL);
  message_timer_=time(NULL);
}

void Koala_main::run(){
  // Error checking
  for (auto client : clients_) {
    int rc = client->loop();
    if (rc) {
      std::cerr << "-- MQTT reconnect" << std::endl;
      client->reconnect();
    }
  }
  
  if(message_received_){
    //Read message
    //draw_matrix_=true;
    std::cout << "Message received on main thread\n";
    read_message();
    if(draw_matrix_){
      std::cout << "Time passed since last update(): "
		<< time(NULL)-upd_timer_ <<"s" << std::endl;
      std::cout << "Time passed since last message(): "
		<< time(NULL)-message_timer_ <<"s" << std::endl;
      time(&upd_timer_);
      time(&message_timer_);
      update();
    }
  }

  if(time(NULL)-upd_timer_ > time_switch_){
    //Switch between the different screens
    if(draw_matrix_)
      update();
  }

  //Check time passed since last message
  for(int i=0; i<ui_->get_nr_nodes(); i++){
    //parse all nodes
    ui_->get_node_timedata(i, interval_, timestamp_);
    if((interval_+INTERVAL_BUFFER) < (time(NULL)-timestamp_)){
      //Node is overdue on messaging
      std::cerr << "Node " << ui_->get_node_ID(i)
		<< " is overdue on messaging by "
		<< ((time(NULL)-timestamp_)-interval_) <<" seconds\n";
      ui_->set_node_state_risk(i, 255);
    }
  }
}

void Koala_main::read_message(){
  message_mutex_.lock();
  std::cout << "Reading " << messages_buffer_.size() << " message(s)\n";
  topic_message_t message;
  if(!(messages_buffer_.empty())){
    message = messages_buffer_.back();
  } else if(messages_buffer_.empty()){
    std::cerr << "Buffer appears empty! Filling buffer seems to have failed!\n";
    message_received_=false;
    return;
  }

  std::cout << "\n*\nRaw message:\n" << message << "\n*\n";

  std::cout << "Organising message\n";
  std::string node_id="";
  float node_latitude=0.0;
  float node_longitude=0.0;
  float node_temp=0.0;
  int node_hum=0;
  int counter=0;
  
  if(message.second[0].find(TTN_DEV_EUI) != message.second[0].end()){
    node_id=message.second[0][TTN_DEV_EUI];
    std::cout << "node id: "<< node_id<<"\n";
    draw_matrix_=true;
  } else {
    std::cout << "No node id found, ignoring message\n";
    message_received_=false;
    while(!messages_buffer_.empty()){
      messages_buffer_.pop_back();
    }
    message_mutex_.unlock();
    return;
  }

  if(message.second[0].find(TTN_META_DATA) != message.second[0].end()){
    node_latitude=message.second[0][TTN_META_DATA][TTN_META_LAT];
    std::cout << "node lat: "<< node_latitude<<"\n";
    node_longitude=message.second[0][TTN_META_DATA][TTN_META_LONG];
    std::cout << "node long: "<< node_longitude<<"\n";
  }

  if(message.second[0].find(TTN_PAYLOAD) != message.second[0].end()){
    std::cout << "node payload fields retrieved\n";
    if(message.second[0][TTN_PAYLOAD].find(TTN_PYLD_TEMP) == message.second[0][TTN_PAYLOAD].end()){
      std::cerr << "Payload fields do not contain " << TTN_PYLD_TEMP << std::endl;
    } else {
      node_temp=(message.second[0][TTN_PAYLOAD][TTN_PYLD_TEMP]);
      //node_temp/=10.0;
    }
    std::cout << "node temp: "<< node_temp <<"\n";
    node_hum=message.second[0][TTN_PAYLOAD][TTN_PYLD_HUM];
    std::cout << "node hum: "<< node_hum<<"\n";
  }

  if(message.second[0].find(TTN_CNTR) != message.second[0].end()){
    counter=message.second[0][TTN_CNTR];
    std::cout << "node counter: "<< counter <<"\n";
  }

  int node_index=ui_->get_node_index(node_id);
  if(node_index==-1){
    std::cout << "Creating new node with dev_eui " << node_id << "\n";
    ui_->add_node(node_id, node_longitude, node_latitude);
    node_index=ui_->get_node_index(node_id);
  }

  std::cout << "Setting node message\n";
  ui_->set_node_message(node_index, node_temp, node_hum, counter);

  messages_buffer_.pop_back();
  if(!(messages_buffer_.empty()))
    std::cerr << "Unread message in the message buffer\n";
  else
    message_received_=false;

  int interval;
  time_t timestamp;
  ui_->get_node_timedata(node_index, interval, timestamp);
  std::cout << "Expecting message from " << node_id
	    << " in " << interval << " seconds / " << interval/3600 << " hours\n";
  write_to_file(node_id, node_temp, node_hum);
  
  message_mutex_.unlock();
}

void Koala_main::update(){
  if(++draw_counter_ > draw_hum)
    draw_counter_=draw_risk;
  
  time(&upd_timer_);

  switch(draw_counter_){
  case draw_risk:
    std::cout << "Drawing risk matrix\n";
    ui_->draw_nodes_risk();
    ui_->print_risk_matrix_SH();
    break;

  case draw_hum:
    std::cout << "Drawing humidity matrix\n";
    ui_->draw_nodes_hum();
    ui_->print_hum_matrix_SH();
    break;

  case draw_temp:
    std::cout << "Drawing temperature matrix\n";
    ui_->draw_nodes_temp();
    ui_->print_temp_matrix_SH();
    break;

  default:
    std::cout << "draw_counter_ out of bounds\n";
    break;
  };
}

void Koala_main::write_to_file(std::string node_id, float node_temp, int node_hum){
  std::stringstream file_data;
  time_t raw_time;
  struct tm* time_info;
  time(&raw_time);
  time_info=localtime(&raw_time);
  
  file_data << "Node: " << node_id << " " << asctime(time_info)
	    << " T" << node_temp << " H" << node_hum << std::endl;

  file_->append(file_data.str());
}
