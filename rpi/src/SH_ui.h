#ifndef SH_UI_H
#define SH_UI_H

#include "SenseHAT_Koala.h"
#include "Pixel.h"
#include "coordinates.h"
#include "node.h"
#include "matrix.h"
#include "TTNconfig.h"
#include "MQTT.h"

#include <iostream>
#include <vector>
#include <string>
#include <time.h>

/*! 
 *  \brief     Class to deal as interface between node and SenseHat
 *  \details   A class used to deal as class to interface between node, SenseHat and matrices.
 *             The class is used to store Node objects, process their states and
 *             values, hand them over to Matrix objects and draw them on the
 *             SenseHat. Uses classes Node, Matrix and SenseHAT_Koala (Composition) and
 *             Coordinates (Aggregation).
 *  \author    Jochem Stevense
 *  \version   1.0
 *  \date      2020
 *  \todo      Remove node state modifiers or move them to private at the least.
 *  \copyright GNU Public License.
 */
class SH_ui{

 public:
  /*!
   * \brief     SenseHat user interface constructor.
   * \details   The Constructor of SH_ui displays a test function on the SenseHat to
   *            check for correct functionality of all pixels.
   */
  SH_ui();

  /*!
   * \brief     Copy constructor.
   * \details   The copy constructor is not allowed and will result in compiler errors.
   */
  SH_ui(SH_ui&) = delete;

  /*!
   * \brief     SH_ui destructor.
   * \details   The destructor displays something on the SenseHat to indicate the end of the 
   *            program, clears the SenseHat and deletes allocated pointers.
   */
  ~SH_ui();

  /*!
   * \brief     Operator "=" overload function.
   * \details   Copying of this class is not allowed.
   */
  SH_ui& operator=(const SH_ui&) = delete;

  /*!
   * \brief     Add a node to the container.
   * \details   Adds a node with a specified ID and location to the container used for the nodes.
   * \param     ID The unique ID/dev_eui/hardware_serial of the Node.
   * \param     x The x location of the device
   * \param     y The y location of the device
   * \warning   Will not check for uniqueness of device ID
   * \todo      Add functionality to test if a node with specified ID already exists. 
   *            Also return a boolean to indicate success or not.
   */
  void add_node(std::string ID, float x, float y);

  /*!
   * \brief     Add a node to the container.
   * \details   Adds a node with a specified ID and location to the container used for the nodes.
   * \param     ID The unique ID/dev_eui/hardware_serial of the Node.
   * \param     coord The coordinates of the device.
   * \warning   Will not check for uniqueness of device ID
   * \todo      Add functionality to test if a node with specified ID already exists. 
   *            Also return a boolean to indicate success or not.
   */
  void add_node(std::string ID, Coordinates& coord);

  /*!
   * \brief     Get the location of the specified node.
   * \details   Gets the location of 
   * \param     index The index of the Node in the container.
   * \returns   Location of the Node as Coordinates.
   * \todo      Overload to enable getting location with device ID.
   */
  Coordinates get_node_location(int index);

  /*!
   * \brief     Get X value of Node.
   * \details   Gets the X value of the location of a specified Node.
   * \param     index The index of the Node in the container.
   * \returns   The X value of the Node.
   */
  float get_node_x(int index);

  /*!
   * \brief     Get Y value of Node.
   * \details   Gets the Y value of the location of a specified Node.
   * \param     index The index of the Node in the container.
   * \returns   The Y value of the Node.
   */
  float get_node_y(int index);

  /*!
   * \brief     Get the index of a node, specified by its location.
   * \details   Get the index of a node, specified by its location.
   * \param     location The location that the Node should have
   * \returns   The index of the Node or -1 if not found.
   */
  int get_node_index(Coordinates& location);

  /*!
   * \brief     Get the index of a node, specified by its location.
   * \details   Get the index of a node, specified by its location.
   * \param     x The x location of the device
   * \param     y The y location of the device
   * \returns   The index of the Node or -1 if not found.
   */
  int get_node_index(float x, float y);

  /*!
   * \brief     Get the index of a node, specified by its device ID.
   * \details   Get the index of a node, specified by its device ID.
   * \param     ID The unique ID of the device.
   * \returns   The index of the Node or -1 if not found.
   */
  int get_node_index(std::string ID);

  /*!
   * \brief     Get the nr of nodes.
   * \details   Gets the number of nodes, currently in the container.
   * \returns   The nr of nodes. Returns 0 if empty.
   */
  int get_nr_nodes();

  /*!
   * \brief     Get Risk state of specified node.
   * \details   Gets the Risk state of a node, specified by its container index.
   * \param     index The index of the Node in the container.
   * \returns   The Risk state of the Node.
   */
  int get_node_state(int index);

  /*!
   * \brief     Get device ID of Node.
   * \details   Gets the device ID of a specified Node.
   * \param     index The index of the Node in the container.
   * \returns   Device ID.
   */
  std::string get_node_ID(int index);

  /*!
   * \brief     Get the time data of a Node.
   * \details   Get the interval & timestamp of a specified node.
   * \param     index The index of the Node in the container.
   * \param     interval Smart pointer to variable to write the interval into.
   * \param     timestamp Smart pointer to variable to write timestamp into.
   */
  void get_node_timedata(int index, int& interval, time_t& timestamp);

  /*!
   * \brief     Set Node values.
   * \details   Sets the adjustable values of a Node, gotten from a message, transmitted by that node.
   * \param     index The index of the Node in the container.
   * \param     temperature Temperature received from message.
   * \param     humidity Humidity received from message.
   * \param     counter Counter received from message.
   */
  void set_node_message(int index, float temperature, int humidity, int counter);

  /*!
   * \brief     Sets the Risk state of a Node.
   * \details   Sets the Risk state of a specified Node.
   * \param     index The index of the Node in the container.
   * \param     state The risk state to set to the Node.
   * \warning   DEPRECATED, setting the state directly will bypass the required 
   *            calculations. Use set_node_message() for desirable results.
   */
  void set_node_state_risk(int index, int state);

  /*!
   * \brief     Sets the Temp state of a Node.
   * \details   Sets the Temp state of a specified Node.
   * \param     index The index of the Node in the container.
   * \param     state The temp state to set to the Node.
   * \warning   DEPRECATED, setting the state directly will bypass the required 
   *            calculations. Use set_node_message() for desirable results.
   */
  void set_node_state_temp(int index, int state);

  /*!
   * \brief     Sets the Hum state of a Node.
   * \details   Sets the Hum state of a specified Node.
   * \param     index The index of the Node in the container.
   * \param     state The hum state to set to the Node.
   * \warning   DEPRECATED, setting the state directly will bypass the required 
   *            calculations. Use set_node_message() for desirable results.
   */
  void set_node_state_hum(int index, int state);

  /*!
   * \brief     Sets the temperature of a node.
   * \details   Sets the temperature of a node.
   * \param     index The index of the Node in the container.
   * \param     temperature Temperature received from message.
   */
  void set_node_temperature(int index, float temperature);

  /*!
   * \brief     Sets the humidity of a node.
   * \details   Sets the humidity of a node.
   * \param     index The index of the Node in the container.
   * \param     humidity Humidity received from message.
   */
  void set_node_humidity(int index, int humidity);

  /*!
   * \brief     Sets the counter of a node.
   * \details   Sets the counter of a node.
   * \param     index The index of the Node in the container.
   * \param     counter Counter received from message.
   */
  void set_node_counter(int index, int counter);

  /*!
   * \brief     Draw a matrix with all nodes and their Risk states on the SenseHat.
   * \details   Draw a matrix with all nodes and their Risk states on the SenseHat.
   */
  void draw_nodes_risk();

  /*!
   * \brief     Draw a matrix with all nodes and their Temp states on the SenseHat.
   * \details   Draw a matrix with all nodes and their Temp states on the SenseHat.
   */
  void draw_nodes_temp();

  /*!
   * \brief     Draw a matrix with all nodes and their Hum states on the SenseHat.
   * \details   Draw a matrix with all nodes and their Hum states on the SenseHat.
   */
  void draw_nodes_hum();

  /*!
   * \brief     Print a matrix with all nodes and their Risk states.
   * \details   Print a matrix with all nodes and their Risk states. This matrix is flexible
   *            in size and will adjust to the number of nodes and their respective location.
   */
  void print_risk_matrix();

  /*!
   * \brief     Print a matrix with all nodes and their Temp states.
   * \details   Print a matrix with all nodes and their Temp states. This matrix is flexible
   *            in size and will adjust to the number of nodes and their respective location.
   */
  void print_temp_matrix();

  /*!
   * \brief     Print a matrix with all nodes and their Hum states.
   * \details   Print a matrix with all nodes and their Hum states.This matrix is flexible
   *            in size and will adjust to the number of nodes and their respective location.
   */
  void print_hum_matrix();

  /*!
   * \brief     Print a matrix with all nodes and their Risk states.
   * \details   Print a matrix with all nodes and their Risk states. This matrix will be 
   *            sized correctly for the SenseHat if possible.
   */
  void print_risk_matrix_SH();

  /*!
   * \brief     Print a matrix with all nodes and their Temp states.
   * \details   Print a matrix with all nodes and their Temp states. This matrix will be 
   *            sized correctly for the SenseHat if possible.
   */
  void print_temp_matrix_SH();

  /*!
   * \brief     Print a matrix with all nodes and their Hum states.
   * \details   Print a matrix with all nodes and their Hum states. This matrix will be 
   *            sized correctly for the SenseHat if possible.
   */
  void print_hum_matrix_SH();

  
 private:
  SenseHAT_Koala* sh_koala_=new SenseHAT_Koala; ///The SenseHat drawing object.
  std::vector<Node*> nodes_={}; ///A container of nodes, dynamic in size.
  Matrix* matrixRisk_=new Matrix; ///The matrix used for the risk levels.
  Matrix* matrixTemp_=new Matrix; ///The matrix used for the temperature levels.
  Matrix* matrixHum_=new Matrix; ///The matrix used for the humidity levels.


  /*!
   * \brief     Transform a risk state into a Pixel.
   * \details   Transforms the given risk state into a Pixel, 
   *            relating to the risk state of the Node.
   * \param     state The risk state of the Node.
   * \returns   A pixel relating to the state of the Node.
   */
  Pixel state_to_pixel(int state);
  
  /*!
   * \brief     Transform a temp state into a Pixel.
   * \details   Transforms the given temp state into a Pixel, 
   *            relating to the temp state of the Node.
   * \param     tstate The temp state of the Node.
   * \returns   A pixel relating to the state of the Node.
   */
  Pixel stateT_to_pixel(int tstate);

  /*!
   * \brief     Transform a hum state into a Pixel.
   * \details   Transforms the given hum state into a Pixel, 
   *            relating to the hum state of the Node.
   * \param     hstate The hum state of the Node.
   * \returns   A pixel relating to the state of the Node.
   */
  Pixel stateH_to_pixel(int hstate);

  /*!
   * \brief     Ensure that the rgb values are usable.
   * \details   Keeps the given rgb values within the bounds of 0 to 255. This is 
   *            to ensure that the values are usable for the SenseHat.
   * \param     red Smart pointer to the given red value.
   * \param     green Smart pointer to the given green value.
   * \param     blue Smart pointer to the given blue value.
   * \returns
   */  
  void ensure_rgb_withing_bounds(int& red, int& green, int& blue);
  
  /*!
   * \brief     Get the maximum X of all nodes.
   * \details   Gets the maximum X value of all current nodes in the container.
   * \returns   The maximum X value.
   */
  float getMaxX();
  
  /*!
   * \brief     Get the maximum Y of all nodes.
   * \details   Gets the maximum Y value of all current nodes in the container.
   * \returns   The maximum Y value.
   */
  float getMaxY();

  /*!
   * \brief     Get the minimum X of all nodes.
   * \details   Gets the minimum X value of all current nodes in the container.
   * \returns   The minimum X value.
   */
  float getMinX();
  
  /*!
   * \brief     Get the minimum Y of all nodes.
   * \details   Gets the minimum Y value of all current nodes in the container.
   * \returns   The minimum Y value.
   */
  float getMinY();

  static const int SH_SIZE; ///The size of the SenseHat LED matrix
  static const int MAX_PIX; ///The maximum value of a colour in a pixel
  static const int MIN_PIX; ///The minimum value of a colour in a pixel
};

#endif
