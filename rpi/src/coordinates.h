#ifndef COORDINATES_H
#define COORDINATES_H

/*! 
 *  \brief     Class to handle coordinates.
 *  \details   Class to handle coordinates.
 *  \author    Jochem Stevense
 *  \version   1.0
 *  \date      2020
 *  \copyright GNU Public License.
 */

class Coordinates{

public:

  /*!
   * \brief     Constructor taking x and y
   * \details   Constructor taking x and y
   * \param     x X coordinate
   * \param     y Y coordinate
   */
  Coordinates(float x, float y): x_(x), y_(y) {};

  /*!
   * \brief     Copy constructor
   * \details   Copy constructor
   * \param     other Other Coordinates
   */
  Coordinates(Coordinates& other): x_(other.getX()), y_(other.getY()){};

  /*!
   * \brief     Empty destructor.
   * \details   Currently does nothing.
   */
  ~Coordinates(){};

  bool operator == (const Coordinates& rhs) const { return (x_==rhs.x_ && y_==rhs.y_);} ///==overload
  Coordinates  operator + (const Coordinates &rhs){ return Coordinates(x_+rhs.x_, y_+rhs.y_);} ///+overload
  Coordinates  operator - (const Coordinates &rhs){return Coordinates(x_-rhs.x_, y_-rhs.y_);}  ///-overload
  Coordinates  operator * (const Coordinates &rhs){return Coordinates(x_*rhs.x_, y_*rhs.y_);}  ///*overload
  Coordinates  operator / (const Coordinates &rhs){return Coordinates(x_/rhs.x_, y_/rhs.y_);}  /// /overload
  Coordinates& operator = (const Coordinates& rhs){x_=rhs.x_; y_=rhs.y_; return* this;} ///=overload
  Coordinates& operator += (const Coordinates &rhs){x_+=rhs.x_; y_+=rhs.y_; return* this;}  ///+=overload
  Coordinates& operator -= (const Coordinates &rhs){x_-=rhs.x_; y_-=rhs.y_; return* this;} ///-=overload
  
  /*!
   * \brief     Set the X coordinate
   * \details   Set the X coordinate
   * \param     x X coordinate 
   */
  void setX(float x){ x_=x; };

    /*!
   * \brief     Set the Y coordinate 
   * \details   Set the Y coordinate 
   * \param     y Y coordinate 
   */
  void setY(float y){ y_=y; };

  /*!
   * \brief     Get the X coordinate
   * \details   Get the X coordinate
   * \returns   The X coordinate 
   */
  float getX(){ return x_; };

  /*!
   * \brief     Get the Y coordinate 
   * \details   Get the Y coordinate 
   * \returns   The Y coordinate 
   */
  float getY(){ return y_; };
  
 private:
  float x_; /// X/longitude coordinate
  float y_; /// Y/latitude coordinate
};

#endif
