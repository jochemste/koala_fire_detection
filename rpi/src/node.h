#ifndef NODE_H
#define NODE_H

#include "coordinates.h"
#include "TTNconfig.h"

#include <string>
#include <time.h>
#include <iostream>

/*! 
 *  \brief     Class to handle and store all Node related data.
 *  \details   This class is used to store and handle all data received from and concerning Nodes.
 *             The class uses Coordinates (Aggregation).
 *  \author    Jochem Stevense
 *  \version   1.0
 *  \date      2020
 *  \warning   Do not manually set the states of a Node
 *  \todo      Move state modifiers to private.
 *  \copyright GNU Public License.
 */
class Node{

 public:
  
  /*!
   * \brief     The Node class constructor.
   * \details   Sets the location and initial timestamp.
   * \param     ID The unique device identifier.
   * \param     x The X value of the location.
   * \param     y The X value of the location.
   */
  Node(std::string ID, float x, float y);

  /*!
   * \brief     The Node class constructor.
   * \details   Sets the location and initial timestamp.
   * \param     ID The unique device identifier.
   * \param     x The  location of the device as Coordinates.
   */
  Node(std::string ID, Coordinates location);

  Node(Node& other) = delete; /// Copy constructor is not allowed.

  /*!
   * \brief     The Node class Destructor.
   * \details   Deletes the allocated member pointers.
   */
  ~Node();

  Node& operator=(const Node&) = delete; /// Copy operator is not allowed.

  /*!
   * \brief     Get the ID of the Node.
   * \details   Gets the device identifier of the Node.
   * \returns   The device identifier.
   */
  std::string getID() const;

  /*!
   * \brief     Sets the Node to active.
   * \details   Sets a boolean to true to indicate that the Node is active.
   * \param     activate The value the active status should get. Default = true;
   */
  void setActive(bool activate=true);
  
  /*!
   * \brief     Returns if node is active.
   * \details   Returns the boolean indicating that the Node is active or not.
   * \returns   The boolean indicating that the Node is active (true) or not (false).
   */
  bool nodeActive();

  /*!
   * \brief     Sets the Risk state of the Node.
   * \details   Sets the Risk state of the Node.
   * \param     state The Risk state the Node should get.
   * \todo      Set to private.
   * \warning   Used by setTemperature() and setHumidity(), should not
   *            be used elsewhere, due to the calculations, done in these functions.
   */
  void setState(int state);
  
  /*!
   * \brief     Sets the Temp state of the Node.
   * \details   Sets the Temp state of the Node.
   * \param     tstate The Temp state the Node should get.
   * \todo      Set to private.
   * \warning   Used by setTemperature(), should not
   *            be used elsewhere, due to the calculations, done in these functions.
   */
  void setTempState(int tState);

  /*!
   * \brief     Sets the Hum state of the Node.
   * \details   Sets the Hum state of the Node.
   * \param     hstate The Hum state the Node should get.
   * \todo      Set to private.
   * \warning   Used by setHumidity(), should not
   *            be used elsewhere, due to the calculations, done in these functions.
   */
  void setHumState(int hState);

  /*!
   * \brief     Sets the temperature of the Node.
   * \details   Sets the temperature of the Node and uses it to set the temp_state 
   *            and risk_state, using the humidity state if possible.
   * \param     temperature The temperature the Node should be set to.
   */
  void setTemperature(float temperature);

  /*!
   * \brief     Sets the humidity of the Node.
   * \details   Sets the humidity of the Node and uses it to set the hum_state 
   *            and risk_state, using the temperature state if possible.
   * \param     humidity The humidity the Node should be set to.
   */
  void setHumidity(int humidity);

  /*!
   * \brief     Sets the counter of the Node message.
   * \details   Sets the counter of the Node message.
   * \param     counter The value the counter should be set to.
   */
  void setCounter(int counter);

  /*!
   * \brief     Set the location of the Node.
   * \details   Set the location of the Node with a Coordinates parameter.
   * \param     newLocation The new location of the Node.
   */
  void setLocation(Coordinates newLocation);

  /*!
   * \brief     Set the location of the Node.
   * \details   Set the location of the Node with a Coordinates parameter.
   * \param     x The new X value of location the Node.
   * \param     y The new Y value of location the Node.
   */
  void setLocation(float x, float y);

  /*!
   * \brief     Sets the timestamp of the node.
   * \details   Sets the timestamp of the node. If no parameter is given, it uses the current time.
   * \param     currentTime The time to be assigned to the timestamp_ member 
   *            variable. Default is the current time.
   */
  void setTimeStamp(time_t currentTime=time(NULL));

  /*!
   * \brief     Get the timestamp of the Node.
   * \details   Gets the timestamp of the Node.
   * \returns   The timestamp of the Node.
   */
  time_t getTimeStamp();

  /*!
   * \brief     Gets the interval of the node.
   * \details   Gets the transmit interval of the node.
   * \returns   The Node transmit interval.
   * \warning   This interval is to be calculated by the private function calculateInterval() 
   *            to match the actual node interval.
   */
  double getInterval();

  /*!
   * \brief     Gets the previous timestamp of the Node.
   * \details   Gets the previous timestamp of the Node.
   * \returns   The previous timestamp of the Node.
   */
  double getPreviousTime();

  /*!
   * \brief     Gets the risk state of the Node.
   * \details   Gets the risk state of the Node.
   * \returns   The risk state of the Node.
   */
  int getState();

  /*!
   * \brief     Gets the temp state of the Node.
   * \details   Gets the temperature state of the Node.
   * \returns   The temperature state of the Node.
   */
  int getTempState();

  /*!
   * \brief     Gets the hum state of the Node.
   * \details   Gets the humidity state of the Node.
   * \returns   The humidity state of the Node.
   */
  int getHumState();

  /*!
   * \brief     Gets the location of the Node.
   * \details   Gets the location of the Node.
   * \returns   The location of the Node as Coordinates.
   */
  Coordinates getLocation();

  /*!
   * \brief     Gets the X location of the Node.
   * \details   Gets the X location of the Node.
   * \returns   The X location of the Node.
   */
  float getLocationX();

  /*!
   * \brief     Gets the Y location of the Node.
   * \details   Gets the Y location of the Node.
   * \returns   The Y location of the Node.
   */
  float getLocationY();

 private:
  const std::string  ID_; ///The unique identifier of the Node.
  bool active_=false; ///The status indicator.

  time_t prev_time_=time(NULL); ///The previous timestamp.
  time_t timestamp_=time(NULL); ///The most current timestamp.
  double interval_=INTERVAL_REGULAR; ///The transmit interval.
  int counter_=0; ///The message counter.
  
  Coordinates* location_; ///The location of the Node.
  int state_=0; ///The risk state of the Node.
  int humidity_=0; ///The humidity of the Node.
  int hum_state_=0; ///The humidity state of the Node.
  float temperature_=0.0; ///The temperature of the Node.
  int temp_state_=0; ///The temperature state of the Node.

  const float rgb_offset_=1.0; ///The Minimum rgb value a Node can have.

  const float temp_slope_=8.467; ///The slope of the temperature state linear formula

  const float rgb_hum_offset_=255.0; ///The Maximum rgb value a Node can have.
  const float hum_slope_=-4.2333; ///The slope of the humidity state linear formula.
  const float hum_offset_=20; ///The minimum humidity value to be processed.

  /*!
   * \brief     Calculate the transmit interval.
   * \details   Calculate the transmit interval, using the same formula as the actual node.
   */
  void calculateInterval();
};

#endif
