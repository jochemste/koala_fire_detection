# Makefile for workflow management

CXX = g++
INCLUDING = -I./_libMQTT -I./_libSenseHAT \
            -I./_libUtils -I./_libUtils/JSON
CXXFLAGS = -std=c++17 -Wall -Wextra -Weffc++ -Wpedantic \
           -Wcast-align -Wcast-qual -Wctor-dtor-privacy \
           -Wdisabled-optimization -Wformat=2 -Winit-self -Wlogical-op \
           -Wmissing-include-dirs -Wnoexcept -Wold-style-cast \
           -Woverloaded-virtual -Wredundant-decls -Wshadow -Wsign-promo \
           -Wstrict-null-sentinel -Wstrict-overflow=5 -Wundef -Wno-unused \
           -Wno-variadic-macros -Wno-parentheses -fdiagnostics-show-option \
           -D_GLIBCXX_USE_NANOSLEEP -fconcepts \
           -Os $(INCLUDING)
LDFLAGS = -lmosquittopp -lmosquitto -lm -pthread

EXECUTABLE = Koala_rpi
SOURCES := ${wildcard *.cpp} \
           ${wildcard *.c} \
           ${wildcard _libMQTT/*.cpp} \
           ${wildcard _libSenseHAT/*.cpp} \
           ${wildcard _libSenseHAT/*.c}
HEADERS := ${wildcard *.h} \
           ${wildcard _libMQTT/*.h} \
           ${wildcard _libUtils/*.h} \
           ${wildcard _libSenseHAT/*.h} \
           ${wildcard _libUtils/JSON/*.hpp}
OBJECTS := ${SOURCES:.cpp=.o}
OBJECTS := ${OBJECTS:.c=.o}

# Code documentation dir DOCS_DIR
DOCS_DIR = "../../docs-rpi"

.PHONY: all
all: ${EXECUTABLE}

$(EXECUTABLE): $(OBJECTS) buildnumber.num
	@echo "---- OBJS: " $(OBJECTS)
	$(CXX) $(CXXFLAGS) $(OBJECTS) $(LDFLAGS) -o $@ -lRTIMULib
	@echo "---- Build: " $$(cat buildnumber.num)

# Create dependency file: use compiler option -MM
depend: $(SOURCES)
	@echo "---- SOURCES info: \n" $(SOURCES) "\n"
	$(CXX) $(CXXFLAGS) -MM  $^ > $@

-include depend

# Buildnumber file
buildnumber.num: $(OBJECTS)
	@if ! test -f buildnumber.num; then echo 0 > buildnumber.num; fi
	@echo $$(($$(cat buildnumber.num)+1)) > buildnumber.num

# Create a clean environment: delete executable and .o files
.PHONY: clean
clean:
	@echo "---- Delete environment"
	$(RM) $(EXECUTABLE) $(OBJECTS)
	@echo "---- Deleted environment\n"

# Clean up environment and dependency file
.PHONY: clean-depend
clean-depend: clean
	@echo "---- Delete depend file"
	$(RM) depend
	@echo "---- Deleted depend file"

# Create documentation
.PHONY: docs
docs: clean-docs
	@echo "---- Generate documentation by Doxygen\n"
	$(shell cd $(DOCS_DIR); doxygen > /dev/null 2>&1)
	@echo "---- Generated documentation by Doxygen\n"

# Clean up generated html code documentation
.PHONY: clean-docs
clean-docs: 
	@echo "---- Delete generated documentation by Doxygen\n"
	$(shell cd $(DOCS_DIR); if [ -d "html" ]; then rm -r html; fi)
	@echo "---- Deleted generated documentation by Doxygen\n"

# Create zip file. 
# Bug: /bin/sh: 1: Syntax error: "(" unexpected  ?? but zip file is created
.PHONY: zip
zip: 
	@echo "---- Create zip file\n"
	$(shell cd ../ ; zip -R ../../../$(EXECUTABLE) \
	        '*.c' '*.cpp' '*.h' '*.hpp' 'Makefile' \
			'Doxyfile' '*.dox' 'images/*.png' \
			'*.puml')
	@echo "---- Created zip file\n"

# Clean all: environment, dependency file and html code documentation
.PHONY: clean-all
clean-all: clean-depend clean-docs

