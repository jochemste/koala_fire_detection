#ifndef APPINFO_H
#define APPINFO_H

#define APPNAME "Koala_rpi" ///Name of the app

#define MAJOR_VERSION "1" ///Major version
#define MINOR_VERSION "0" ///Minor version
#define REVISION_VERSION "0" ///Revision version
#define VERSION MAJOR_VERSION "." MINOR_VERSION "." REVISION_VERSION ///Complete version
#define APPNAME_VERSION APPNAME " v" VERSION ///App name with version

#endif
