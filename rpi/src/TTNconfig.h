#ifndef TTNCONFIG_H
#define TTNCONFIG_H

#include <string>

const std::string TTN_APP_ID{"1920_koala_forest_fire_detection"}; /// App ID in TTN
const std::string TTN_ACC_KEY{"ttn-account-v2.3jptgN-J6hcph7_zUir0jEadjkvUMbF-EBomQvE9Cxk"}; /// Access key for device
const std::string TTN_REGION{"eu"}; /// ttn-handler-eu

const std::string TTN_MQTT_HOST{TTN_REGION + ".thethings.network"}; ///TTN host
const int TTN_MQTT_PORT{1883}; ///TTN MQTT port
const int TTN_KEEP_ALIVE{1200}; ///Seconds to keep alive when idle

const std::string TTN_TOPIC_ACTIVATIONS{"+/devices/+/events/activations"}; ///The topic to check for activations
const std::string TTN_TOPIC_UP{"+/devices/+/up"}; ///The topic to listen to for uplink messages

const std::string TTN_DEV_ID{"test_device_jas"};
const std::string TTN_DEV_EUI{"hardware_serial"}; /// dev_eui/Node ID
const std::string TTN_PAYLOAD_FIELD{"analog_in_50"}; /// payload field
const std::string TTN_PAYLOAD{"payload_fields"}; /// All payload fields
const std::string TTN_PYLD_TEMP{"temperature_1"}; /// temperature field
const std::string TTN_PYLD_HUM{"relative_humidity_2"}; /// humidity field
const std::string TTN_CNTR{"counter"}; /// uplink counter
const std::string TTN_META_DATA{"metadata"}; /// message metadata
const std::string TTN_META_LAT{"latitude"}; /// latitude field
const std::string TTN_META_LONG{"longitude"}; /// longitude field
const double TTN_DATA_MAX{100}; ///
const double TTN_DATA_MIN{-100}; ///
const std::string TTN_DATA_NAME{"sine (-100 ... +100)"}; ///

const int INTERVAL_REGULAR{60}; /// The default interval
const int INTERVAL_BUFFER{10}; /// The buffertime given to the node to deviate from the calculations

#endif
