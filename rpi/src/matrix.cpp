#include "matrix.h"

Matrix::Matrix(){

}

Matrix::~Matrix(){

}

void Matrix::initMatrix(float minX, float maxX, float minY,
			float maxY, int size){
  addGrid(size);
  initColCoords(minX, maxX);
  initRowCoords(minY, maxY);
}

bool Matrix::fitsInMatrix(float x, float y){
  int size=getSize();
  int column=fitsInColumn(x);
  int row=fitsInRow(y);

  if((column < size) && (row < size))
    return true;

  return false;
}

int Matrix::fitsInColumn(float x){
  int index=-1;
  for(auto& columnCoord: columnCoords_){
    if(x<columnCoord){
      return index;
    }
    index++;
  }
  return index;
}

int Matrix::fitsInRow(float y){
  int index=-1;
  for(auto& rowCoord: rowCoords_){
    if(y<rowCoord){
      return index;
    }
    index++;
  }
  return index;
}

void Matrix::addGrid(){
  addColumn();
  addRow();
}

void Matrix::addGrid(int nr){
  addColumn(nr);
  addRow(nr);
}

void Matrix::addRow(){
  for(auto& el: elements_){
    el.push_back(0);
  }
  nrRows_++;
}

void Matrix::addRow(int nr){
  for(auto& el: elements_){
    for(auto i=0; i<nr; i++){
      el.push_back(0);
    }
  }
  nrRows_+=nr;
}

void Matrix::addColumn(){
  std::vector<int> rows;
  for(int i=0; i<nrRows_; i++)
    rows.push_back(0);
  elements_.push_back(rows);
  nrColumns_++;
}

void Matrix::addColumn(int nr){
  for(auto i=0; i<nr; i++){
    std::vector<int> rows;
    for(int ctr=0; ctr<nrRows_; ctr++)
      rows.push_back(0);
    elements_.push_back(rows);
  }

  nrColumns_+=nr;
}

int Matrix::getNrColumns(){
  return nrColumns_;
}

int Matrix::getNrRows(){
  return nrRows_;
}

int Matrix::getElementState(int column, int row){
  return elements_[column][row];
}

void Matrix::setElementState(int state, int column, int row){
  elements_[column][row] = state;
}

int Matrix::getSize(){
  if(nrRows_!=nrColumns_){
    std::cerr << "Nr of rows("<<nrRows_<<") and nr of columns("
	      <<nrColumns_<<") are not equal!\n";
    return -1;
  }

  return nrRows_;
}

void Matrix::clear(){
  elements_.clear();
  columnCoords_.clear();
  rowCoords_.clear();
  nrRows_=0;
  nrColumns_=0;
}

Matrix Matrix::transformToMatrix(int size){
  Matrix newMatrix;
  int difference = size-nrRows_;
  int leftSpace=0;
  int rightSpace=0;
  while(newMatrix.getSize()<nrRows_){
    newMatrix.addGrid();
  }

  while(newMatrix.getSize()<size)
    newMatrix.addGrid();

  if(difference>0){
    leftSpace = difference/2;
    rightSpace = difference-leftSpace;
  } else if (difference<=0){
    
    leftSpace=0;
    rightSpace=0;
  }
  
  for(int row=0; row<nrRows_; row++){
    for(int column=0; column<nrColumns_; column++){
      newMatrix.setElementState(this->getElementState(column, row),
				column+leftSpace, row+leftSpace);
    }
  }

  newMatrix.eraseEmptyGrids(size);
  
  return newMatrix;
}

void Matrix::printMatrix(){

  for(int row=0; row<nrRows_; row++){
    for(int column=0; column<nrColumns_; column++){
      std::cout << getElementState(column, row) << " | ";
    }
    std::cout << "\n";
  }
}

void Matrix::printTransformedMatrix(int size){
  Matrix transMatrix=this->transformToMatrix(size);
  transMatrix.printMatrix();
}

//PRIVATE//////////////

void Matrix::initColCoords(float minX, float maxX){
  columnCoords_.clear();
  int nrElements=elements_.size();
  float elementSize=(maxX-minX)/nrElements;

  for(int i=0; i<nrElements; i++){
    columnCoords_.push_back(minX+(i*elementSize));
  }
}

void Matrix::initRowCoords(float minY, float maxY){
  rowCoords_.clear();
  int nrElements=elements_.size();
  float elementSize=(maxY-minY)/nrElements;

  for(int i=0; i<nrElements; i++){
    rowCoords_.push_back(minY+(i*elementSize));
  }
}

void Matrix::eraseEmptyGrids(int desiredSize){
  if(!(desiredSize>getSize())){      
    std::vector<int> eraseRows={};
    std::vector<int> eraseColumns={};

    //Check rows
    for(int row=0; row<nrRows_; row++){
      bool eraseRow=true;
      for(int column=0; column<nrColumns_; column++){
	if(getElementState(column, row)>0)
	  eraseRow=false;
      }
      if(eraseRow)
	eraseRows.push_back(row);
    }

    //Check columns
    for(int column=0; column<nrColumns_; column++){
      bool eraseColumn=true;
      for(int row=0; row<nrRows_; row++){
	if(!(getElementState(column, row)==0))
	  eraseColumn=false;
      }
      if(eraseColumn)
	eraseColumns.push_back(column);
    }

    //Erase columns and rows
    //Set ctr to last element
    unsigned int ctr=0;
    if(eraseColumns.size()<=eraseRows.size())
      ctr=eraseColumns.size()-1;
    else
      ctr=eraseRows.size()-1;

    //Erase loop
    while(getSize()>desiredSize){
      if((elements_.empty())){
	std::cerr << "elements_ reduced to "
		  << getSize() << std::endl;
	break;
      }

      //Erase grids
      if(!(ctr>eraseRows.size()) && !(ctr>eraseColumns.size())){
	//Erase row
	for(auto& el: elements_){
	  el.erase(el.begin()+eraseRows[ctr]);
	}
	nrRows_--;
      
	//Erase column
	elements_.erase(elements_.begin()+eraseColumns[ctr]);
	nrColumns_--;
      }

      if(ctr==0)
	break;
      else
	ctr--;
    }
  }
}
