#include "file.h"

File::File(){

}

File::~File(){

}

void File::set_fd(std::string fd){
  fd_=fd;
}

void File::append(std::string data){
  outfile_.open(fd_, std::ios_base::app);
  outfile_ << data.c_str();
  outfile_.close();
}

void File::erase_all(){
  outfile_.open(fd_, std::ofstream::out | std::ofstream::trunc);
  outfile_.close();
}

int File::read_int(){
  std::ifstream infile(fd_);
  int data;
  while (infile >> data);
  return data;
}
