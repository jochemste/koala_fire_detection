#!/usr/bin/python3

import time, sys, os, subprocess, datetime

executable= "./Koala_rpi"
log_file= "python_log/python_log.txt"

ctr=0
sig_Cc=25
sig_KILL=9
log_string=""
error=False

process = subprocess.Popen([executable])

print("\nPYTH-> " +executable + " started")
log_string+=str(datetime.datetime.now()) + ': Starting\n'

#process = subprocess.Popen([executable], stderr=subprocess.PIPE)

try:
    while(True):
        if process.poll() == None:
            time.sleep(1)
        else:
            print("\nPYTH-> Restarting " + executable)
            log_string+=str(datetime.datetime.now()) + ': Restarting\n'
            process = subprocess.Popen([executable])
            ctr+=1

except KeyboardInterrupt:
    os.kill(0, sig_Cc)
    
    
with open(log_file, 'w') as fd:
    fd.truncate(0)
    log_string+=str(datetime.datetime.now()) + ': The number of restarts:'+str(ctr)+'\n'
    fd.write(log_string)

time.sleep(5)
while( process.poll()==None):
    print('\nERROR: Process did not end!')
    log_string+='ERROR: '+str(datetime.datetime.now())+': Process did not end!'
    os.kill(0, sig_KILL)
    error=True
    time.sleep(1)

    if error:
        with open(log_file, 'w') as fd:
            fd.truncate(0)
            log_string+=str(datetime.datetime.now()) + ': The number of restarts:'+str(ctr)+'\n'
            fd.write(log_string)
