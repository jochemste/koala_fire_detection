#ifndef KOALA_MAIN_H
#define KOALA_MAIN_H

#include "MQTT.h"
#include "SH_ui.h"
#include "AppInfo.h"
#include "TTNconfig.h"
#include "file.h"

#include <json.hpp>
#include <mosquittopp.h>
#include <mutex>
#include <iostream>
#include <string>
#include <vector>
#include <time.h>
#include <sstream>

using json = nlohmann::json;

/*! 
 *  \brief     Class to handle main process of Koala_rpi
 *  \details   This class is used to handle the main processes
               of the program. 
 *             The program uses objects from classes MQTT, 
               SH_ui and File (Composition).
 *  \author    Jochem Stevense
 *  \version   1.0
 *  \date      2020
 *  \warning   First call init() before run().
 *  \todo      Create a boolean to indicate whether or not init() has been run. 
               If not, the program should exit.
 *  \copyright GNU Public License.
 */
class Koala_main{
 public:
  using topic_message_t = std::pair<std::string, json>;

  /*!
   * \brief     The Class constructor
   * \details   Creates a new file every build for data to be written into.
   */
  Koala_main();

  Koala_main(const Koala_main&)=delete; /// Copy constructor is not allowed.

  /*!
   * \brief     The Class destructor
   * \details   Cleans up mosquitto library and deletes allocated pointer.
   */
  ~Koala_main();

  Koala_main& operator=(const Koala_main& )=delete; /// Copy operator not allowed.

  /*!
   * \brief     Initialise the program.
   * \details   Initialise mosquitto, MQTT and set the timers
   *            to the current time.
   */
  void init();

  /*!
   * \brief     Run the program.
   * \details   Run the program. Check for MQTT errors, transmitted
   *            messages, and check if any of the nodes is overdue
   *            on messaging. Display a different type of matrix (risk,
   *            temp, hum) every 20 seconds. The current matrix is
   *            indicated before displaying, by displaying the first 
   *            letter of the type of matrix (R, T, H).
   * \warning   Run init() before running this function.
   */
  void run();

 private:
  SH_ui* ui_=new SH_ui; /// The interface between nodes, matrix and the sensehat.
  MQTT* mqtt_; /// Object to start a second thread to be able to receive messages from the nodes.
  File* file_=new File; /// Object to handle writing into a file, to log information.
  std::vector<mosqpp::mosquittopp *> clients_; /// Used for MQTT
  time_t upd_timer_=time(NULL); /// Update Sensehat timer
  time_t message_timer_ = time(NULL); /// Message received timer
  time_t timestamp_ = time(NULL); /// Member variable to receive node time stamp
  int interval_=INTERVAL_REGULAR; /// Member variable to receive node interval

  std::vector<topic_message_t> messages_buffer_={}; /// Vector to store messages.
  std::mutex message_mutex_; /// Mutex to ensure threadsafe reading/writing buffer.
  bool message_received_=false; /// Boolean to indicate that a message is received.
  bool draw_matrix_=false; /// Boolean to make sure a matrix is only drawn when nodes are present.

  int draw_counter_=-1; /// A counter to switch between the different sensehat matrices.

  const std::string className_; /// Member variable to store the classname

  const double time_switch_=20; /// The time in seconds between switching of sensehat matrices.
  const std::string directory_="data_files/"; /// Directory to write data to
  const std::string file_name_="data"; /// Base of the name of the file to write to
  const std::string extension_=".txt"; /// Extension of the file to write to
  const std::string buildnr_file_="buildnumber.num"; /// File that indicates the current build nr
  std::string full_file_name_=""; /// Full file name to write data to

  enum{
       draw_risk=0,
       draw_temp,
       draw_hum
  }; /// Enum to allow a counter to indicate a matrix to be drawn.

  /*!
   * \brief     Private function to read a message.
   * \details   Private function to read a message from the buffer and
   *            empty it again. Uses \a ui_ to organise the data to be
   *            stored for the nodes.
   */
  void read_message();

  /*!
   * \brief     Private function that updates the Sensehat.
   * \details   Updates the Sensehat by using \a ui_.
   */
  void update();

  /*!
   * \brief     Private function to write data to a file.
   * \details   Appends data to a file, including a timestamp.
   * \param     node_id The ID/dev_eui to be written into the file.
   * \param     temp The temperature to be written into the file.
   * \param     hum The humidity to be written into the file.
   */
  void write_to_file(std::string node_id, float temp, int hum);
};

#endif
