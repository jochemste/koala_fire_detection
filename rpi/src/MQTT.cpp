#include "MQTT.h"
#include "MQTTconfig.h"
#include "MQTTmessage.h"
#include "TTNconfig.h"
#include "Topic.h"
#include "json.hpp"

#include <iostream>
#include <mosquitto.h>
#include <mosquittopp.h>
#include <string>

#define CERR std::cerr << '\n' << className_ << "::" << __func__ << "()\n   "

MQTT::MQTT(const std::string &appname, const std::string &clientname,
             const std::string &host, int port)
   : mosqpp::mosquittopp{(HOSTNAME + appname + clientname).c_str()}
   , className_{__func__}
   , mqttID_{HOSTNAME + appname + clientname}
   , queue_{bind(&MQTT::handle, this, std::placeholders::_1)}
{
   CERR << "connect() host = '" << host << "'  port = " << port
        << "  MQTTid = " << mqttID_ << std::endl;
   username_pw_set(TTN_APP_ID.c_str(), TTN_ACC_KEY.c_str());
   connect(host.c_str(), port, TTN_KEEP_ALIVE);
}

MQTT::~MQTT()
{
   disconnect();
   CERR << " disconnect()" << std::endl;
}

void MQTT::init_messages(std::mutex& mut, std::vector<topic_message_t>& mess, bool& rec){
  message_mutex_ptr_=&mut;
  messages_ptr_=&mess;
  message_received_ptr_=&rec;
}

void MQTT::on_connect(int rc)
{
   CERR << "connected with rc = " << rc << " '" << mosquitto_strerror(rc) << "'"
        << std::endl;
   if (rc == 0) {
      /// Only attempt to subscribe on a successful connect.
      int rc1 = subscribe(nullptr, TTN_TOPIC_UP.c_str(), MQTT_QoS_0);
      CERR << " " << TTN_TOPIC_UP << std::endl;
      if (rc1 != MOSQ_ERR_SUCCESS) {
         on_log(1, mosquitto_strerror(rc1));
      }
      int rc2 = subscribe(nullptr, TTN_TOPIC_ACTIVATIONS.c_str(), MQTT_QoS_0);
      CERR << " " << TTN_TOPIC_ACTIVATIONS << std::endl;
      if (rc2 != MOSQ_ERR_SUCCESS) {
         on_log(1, mosquitto_strerror(rc2));
      }
   }
}

void MQTT::on_disconnect(int rc)
{
   CERR << " disconnected with rc = " << rc << " '" << mosquitto_strerror(rc)
        << "'" << std::endl;
}

void MQTT::on_message(const mosquitto_message *message)
{
  CERR << " receiving message\n";
  MQTTmessage msg(message);
  message_transmit_=true;
  
  json jsonMsg{json::parse(msg.getPayload())};
  auto tm = make_pair(msg.getTopic(), jsonMsg);
  queue_.provide(tm);
}

void MQTT::on_subscribe(int mid, int qos_count, const int *granted_qos)
{
   CERR << "Subscription succeeded, mid = " << mid
        << " qos_count = " << qos_count << " granted_qos = " << *granted_qos
        << std::endl;
}

void MQTT::on_log(int level, const char *str)
{
   CERR << " level = " << level << ": " << str << std::endl;
}

void MQTT::on_error()
{
   CERR << " ERROR " << std::endl;
}


//PRIVATE//////////////////////////

void MQTT::handle(const topic_message_t &topic_message){
  
  topic_message_t message=topic_message;

  if(message_transmit_){
    message_transmit_=false;

    // Thread safe messaging
    message_mutex_ptr_->lock();
    CERR << "Adding message to buffer\n";
    messages_ptr_->push_back(message);
    CERR << messages_ptr_->size() << " message(s) in buffer\n";
    *message_received_ptr_=true;
    message_mutex_ptr_->unlock();
    CERR << "Done adding message to buffer\n";
  }
}
