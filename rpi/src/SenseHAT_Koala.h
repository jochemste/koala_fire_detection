#ifndef SENSEHAT_KOALA_H
#define SENSEHAT_KOALA_H

#include "Pixel.h"
#include "SenseHAT.h"
#include <chrono>
#include <thread>
#include <time.h>

/*! 
 *  \brief     Class to handle SenseHat drawing for the Koala fire detection system.
 *  \details   This class is used to simplify drawing, clearing and toggle pixels on the SenseHat.
 *             This class is a public derivation of SenseHAT
 *  \author    Jochem Stevense
 *  \version   1.0
 *  \date      2020
 *  \copyright GNU Public License.
 */
class SenseHAT_Koala : public SenseHAT
{
public:
  /*!
   * \brief     SenseHat_Koala constructor.
   * \details   The constructor clears the SenseHat of all 
   *            pixels that might still have been set previously.
   */
  SenseHAT_Koala();
  virtual ~SenseHAT_Koala() = default;

  /*!
   * \brief     Draw pixels to test pixels.
   * \details   A loop that draws pixels one by one in the background colour. 
   *            This allows the user to see if all LEDs on the SenseHat work;
   */
  void drawTest();

  
  /*!
   * \brief     Draw pixels to indicate end of program.
   * \details   A loop that sets pixels from background colour to off as a 
   *            nice way to end a program.
   */
  void drawEnd();

  
  /*!
   * \brief     Draw a pixel on the SenseHat.
   * \details   Draw a pixel in a specified row and column with a given rgb.
   * \param     column The column nr to draw in.
   * \param     row The row nr to draw in.
   * \param     r The intensity of red as an integer between 0-255.
   * \param     g The intensity of green as an integer between 0-255.
   * \param     b The intensity of blue as an integer between 0-255.
   */
  void draw(int column, int row, int r, int g, int b);

  
  /*!
   * \brief     Draw a pixel on the SenseHat.
   * \details   Draw a pixel in a specified row and column with a given rgb.
   * \param     column The column nr to draw in.
   * \param     row The row nr to draw in.
   * \param     pixel A predefined pixel to use as rgb values.
   */
  void draw(int column, int row, Pixel& pixel);

  
  /*!
   * \brief     Draw letter "R".
   * \details   A function to write the letter "R" for a given time to the SenseHat.
   * \param     duration The duration in seconds to display the letter
   * \warning   Thread blocking function! Will block the thread until the end of the duration.
   */
  void drawR(int duration);

  
  /*!
   * \brief     Draw letter "T".
   * \details   A function to write the letter "T" for a given time to the SenseHat.
   * \param     duration The duration in seconds to display the letter
   * \warning   Thread blocking function! Will block the thread until the end of the duration.
   */
  void drawT(int duration);

  
  /*!
   * \brief     Draw letter "H".
   * \details   A function to write the letter "H" for a given time to the SenseHat.
   * \param     duration The duration in seconds to display the letter
   * \warning   Thread blocking function! Will block the thread until the end of the duration.
   */
  void drawH(int duration);

  
  /*!
   * \brief     Toggle pixel in specified location.
   * \details   Toggles the pixel in the specified row and column by checking the current Pixel 
   *            state with the given one. If they are equal, the pixel will be set
   *            to background colour. If not, the pixel will be set to 
   *            the specified colour.
   * \param     column The column nr to draw in.
   * \param     row The row nr to draw in.
   * \param     r The intensity of red as an integer between 0-255.
   * \param     g The intensity of green as an integer between 0-255.
   * \param     b The intensity of blue as an integer between 0-255.
   */
  void toggle(int column, int row, int r, int g, int b);

  
  /*!
   * \brief     Toggle pixel in specified location.
   * \details   Toggles the pixel in the specified row and column by checking the current Pixel 
   *            state with the given one. If they are equal, the pixel will be
   *            set to background colour. If not, the pixel will be set to the specified colour.
   * \param     column The column nr to draw in.
   * \param     row The row nr to draw in.
   * \param     pixel A predefined pixel to use as rgb values.
   */
  void toggle(int column, int row, Pixel& pixel);

  
  /*!
   * \brief     Clear the SenseHat.
   * \details   Clears the SenseHat and sets it to the specified rgb colour.
   * \param     r The intensity of red as an integer between 0-255.
   * \param     g The intensity of green as an integer between 0-255.
   * \param     b The intensity of blue as an integer between 0-255.
   */
  void clear(int r, int g, int b);

  
  /*!
   * \brief     Clear the SenseHat.
   * \details   Clears the SenseHat and sets it to the specified rgb colour.
   * \param     pixel A predefined pixel to use as rgb values.
   */
  void clear(Pixel& pixel);

  
  /*!
   * \brief     Clear the SenseHat.
   * \details   Clears the SenseHat and turns all LEDs off.
   */
  void clear();
  
private:
  const static Pixel background_; /// The default background colour
  const static Pixel letters_; /// The default colour of letters
  
  const int columns_=8; /// The number of columns on the SenseHat
  const int rows_=8; /// The number of rows on the SenseHat
  
};

#endif
