var hierarchy =
[
    [ "nlohmann::adl_serializer< typename, typename >", "structnlohmann_1_1adl__serializer.html", null ],
    [ "B1", null, [
      [ "nlohmann::detail::conjunction< B1 >", "structnlohmann_1_1detail_1_1conjunction_3_01B1_01_4.html", null ]
    ] ],
    [ "nlohmann::basic_json< ObjectType, ArrayType, StringType, BooleanType, NumberIntegerType, NumberUnsignedType, NumberFloatType, AllocatorType, JSONSerializer >", "classnlohmann_1_1basic__json.html", null ],
    [ "nlohmann::detail::binary_reader< BasicJsonType, SAX >", "classnlohmann_1_1detail_1_1binary__reader.html", null ],
    [ "nlohmann::detail::binary_writer< BasicJsonType, CharType >", "classnlohmann_1_1detail_1_1binary__writer.html", null ],
    [ "nlohmann::detail::dtoa_impl::boundaries", "structnlohmann_1_1detail_1_1dtoa__impl_1_1boundaries.html", null ],
    [ "nlohmann::detail::dtoa_impl::cached_power", "structnlohmann_1_1detail_1_1dtoa__impl_1_1cached__power.html", null ],
    [ "Coordinates", "classCoordinates.html", null ],
    [ "nlohmann::detail::detector< Default, AlwaysVoid, Op, Args >", "structnlohmann_1_1detail_1_1detector.html", null ],
    [ "nlohmann::detail::detector< Default, void_t< Op< Args... > >, Op, Args... >", "structnlohmann_1_1detail_1_1detector_3_01Default_00_01void__t_3_01Op_3_01Args_8_8_8_01_4_01_4_00_01Op_00_01Args_8_8_8_01_4.html", null ],
    [ "nlohmann::detail::dtoa_impl::diyfp", "structnlohmann_1_1detail_1_1dtoa__impl_1_1diyfp.html", null ],
    [ "std::exception", null, [
      [ "nlohmann::detail::exception", "classnlohmann_1_1detail_1_1exception.html", [
        [ "nlohmann::detail::invalid_iterator", "classnlohmann_1_1detail_1_1invalid__iterator.html", null ],
        [ "nlohmann::detail::other_error", "classnlohmann_1_1detail_1_1other__error.html", null ],
        [ "nlohmann::detail::out_of_range", "classnlohmann_1_1detail_1_1out__of__range.html", null ],
        [ "nlohmann::detail::parse_error", "classnlohmann_1_1detail_1_1parse__error.html", null ],
        [ "nlohmann::detail::type_error", "classnlohmann_1_1detail_1_1type__error.html", null ]
      ] ]
    ] ],
    [ "nlohmann::detail::external_constructor< value_t >", "structnlohmann_1_1detail_1_1external__constructor.html", null ],
    [ "nlohmann::detail::external_constructor< value_t::array >", "structnlohmann_1_1detail_1_1external__constructor_3_01value__t_1_1array_01_4.html", null ],
    [ "nlohmann::detail::external_constructor< value_t::boolean >", "structnlohmann_1_1detail_1_1external__constructor_3_01value__t_1_1boolean_01_4.html", null ],
    [ "nlohmann::detail::external_constructor< value_t::number_float >", "structnlohmann_1_1detail_1_1external__constructor_3_01value__t_1_1number__float_01_4.html", null ],
    [ "nlohmann::detail::external_constructor< value_t::number_integer >", "structnlohmann_1_1detail_1_1external__constructor_3_01value__t_1_1number__integer_01_4.html", null ],
    [ "nlohmann::detail::external_constructor< value_t::number_unsigned >", "structnlohmann_1_1detail_1_1external__constructor_3_01value__t_1_1number__unsigned_01_4.html", null ],
    [ "nlohmann::detail::external_constructor< value_t::object >", "structnlohmann_1_1detail_1_1external__constructor_3_01value__t_1_1object_01_4.html", null ],
    [ "nlohmann::detail::external_constructor< value_t::string >", "structnlohmann_1_1detail_1_1external__constructor_3_01value__t_1_1string_01_4.html", null ],
    [ "false_type", null, [
      [ "nlohmann::detail::is_basic_json< typename >", "structnlohmann_1_1detail_1_1is__basic__json.html", null ],
      [ "nlohmann::detail::is_compatible_integer_type_impl< bool, typename, typename >", "structnlohmann_1_1detail_1_1is__compatible__integer__type__impl.html", null ],
      [ "nlohmann::detail::is_compatible_object_type_impl< B, RealType, CompatibleObjectType >", "structnlohmann_1_1detail_1_1is__compatible__object__type__impl.html", null ],
      [ "nlohmann::detail::is_compatible_string_type_impl< B, RealType, CompatibleStringType >", "structnlohmann_1_1detail_1_1is__compatible__string__type__impl.html", null ],
      [ "nlohmann::detail::is_complete_type< T, typename >", "structnlohmann_1_1detail_1_1is__complete__type.html", null ]
    ] ],
    [ "File", "classFile.html", null ],
    [ "nlohmann::detail::from_json_fn", "structnlohmann_1_1detail_1_1from__json__fn.html", null ],
    [ "nlohmann::detail::has_from_json< BasicJsonType, T >", "structnlohmann_1_1detail_1_1has__from__json.html", null ],
    [ "nlohmann::detail::has_non_default_from_json< BasicJsonType, T >", "structnlohmann_1_1detail_1_1has__non__default__from__json.html", null ],
    [ "nlohmann::detail::has_to_json< BasicJsonType, T >", "structnlohmann_1_1detail_1_1has__to__json.html", null ],
    [ "std::hash< nlohmann::json >", "structstd_1_1hash_3_01nlohmann_1_1json_01_4.html", null ],
    [ "nlohmann::detail::index_sequence< Ints >", "structnlohmann_1_1detail_1_1index__sequence.html", null ],
    [ "nlohmann::detail::index_sequence< 0 >", "structnlohmann_1_1detail_1_1index__sequence.html", [
      [ "nlohmann::detail::make_index_sequence< 1 >", "structnlohmann_1_1detail_1_1make__index__sequence_3_011_01_4.html", null ]
    ] ],
    [ "nlohmann::detail::index_sequence< I1...,(sizeof...(I1)+I2)... >", "structnlohmann_1_1detail_1_1index__sequence.html", [
      [ "nlohmann::detail::merge_and_renumber< index_sequence< I1... >, index_sequence< I2... > >", "structnlohmann_1_1detail_1_1merge__and__renumber_3_01index__sequence_3_01I1_8_8_8_01_4_00_01inde4885d6f1d93a04f25932afbd429c4793.html", null ]
    ] ],
    [ "nlohmann::detail::index_sequence<>", "structnlohmann_1_1detail_1_1index__sequence.html", [
      [ "nlohmann::detail::make_index_sequence< 0 >", "structnlohmann_1_1detail_1_1make__index__sequence_3_010_01_4.html", null ]
    ] ],
    [ "INIreader", "classINIreader.html", null ],
    [ "nlohmann::detail::input_adapter", "classnlohmann_1_1detail_1_1input__adapter.html", null ],
    [ "nlohmann::detail::input_adapter_protocol", "structnlohmann_1_1detail_1_1input__adapter__protocol.html", [
      [ "nlohmann::detail::input_buffer_adapter", "classnlohmann_1_1detail_1_1input__buffer__adapter.html", null ],
      [ "nlohmann::detail::input_stream_adapter", "classnlohmann_1_1detail_1_1input__stream__adapter.html", null ],
      [ "nlohmann::detail::wide_string_input_adapter< WideStringType >", "classnlohmann_1_1detail_1_1wide__string__input__adapter.html", null ]
    ] ],
    [ "integral_constant", null, [
      [ "nlohmann::detail::negation< B >", "structnlohmann_1_1detail_1_1negation.html", null ]
    ] ],
    [ "nlohmann::detail::internal_iterator< BasicJsonType >", "structnlohmann_1_1detail_1_1internal__iterator.html", null ],
    [ "nlohmann::detail::internal_iterator< typename std::remove_const< BasicJsonType >::type >", "structnlohmann_1_1detail_1_1internal__iterator.html", null ],
    [ "iotShieldButton", "classiotShieldButton.html", null ],
    [ "iotShieldLED", "classiotShieldLED.html", null ],
    [ "iotShieldPotmeter", "classiotShieldPotmeter.html", null ],
    [ "iotShieldTempSensor", "classiotShieldTempSensor.html", null ],
    [ "nlohmann::detail::is_basic_json_nested_type< BasicJsonType, T >", "structnlohmann_1_1detail_1_1is__basic__json__nested__type.html", null ],
    [ "nlohmann::detail::is_compatible_array_type< BasicJsonType, CompatibleArrayType >", "structnlohmann_1_1detail_1_1is__compatible__array__type.html", null ],
    [ "nlohmann::detail::is_compatible_complete_type< BasicJsonType, CompatibleCompleteType >", "structnlohmann_1_1detail_1_1is__compatible__complete__type.html", null ],
    [ "nlohmann::detail::is_compatible_integer_type< RealIntegerType, CompatibleNumberIntegerType >", "structnlohmann_1_1detail_1_1is__compatible__integer__type.html", null ],
    [ "nlohmann::detail::is_compatible_integer_type_impl< true, RealIntegerType, CompatibleNumberIntegerType >", "structnlohmann_1_1detail_1_1is__compatible__integer__type__impl_3_01true_00_01RealIntegerType_0064332c4ada80cab3523aebd66ccc012a.html", null ],
    [ "nlohmann::detail::is_compatible_object_type< BasicJsonType, CompatibleObjectType >", "structnlohmann_1_1detail_1_1is__compatible__object__type.html", null ],
    [ "nlohmann::detail::is_compatible_object_type_impl< true, RealType, CompatibleObjectType >", "structnlohmann_1_1detail_1_1is__compatible__object__type__impl_3_01true_00_01RealType_00_01CompatibleObjectType_01_4.html", null ],
    [ "nlohmann::detail::is_compatible_string_type< BasicJsonType, CompatibleStringType >", "structnlohmann_1_1detail_1_1is__compatible__string__type.html", null ],
    [ "nlohmann::detail::is_compatible_string_type_impl< true, RealType, CompatibleStringType >", "structnlohmann_1_1detail_1_1is__compatible__string__type__impl_3_01true_00_01RealType_00_01CompatibleStringType_01_4.html", null ],
    [ "nlohmann::detail::is_sax< SAX, BasicJsonType >", "structnlohmann_1_1detail_1_1is__sax.html", null ],
    [ "nlohmann::detail::is_sax_static_asserts< SAX, BasicJsonType >", "structnlohmann_1_1detail_1_1is__sax__static__asserts.html", null ],
    [ "nlohmann::detail::iter_impl< BasicJsonType >", "classnlohmann_1_1detail_1_1iter__impl.html", null ],
    [ "nlohmann::detail::iteration_proxy< IteratorType >", "classnlohmann_1_1detail_1_1iteration__proxy.html", null ],
    [ "nlohmann::detail::iteration_proxy< IteratorType >::iteration_proxy_internal", "classnlohmann_1_1detail_1_1iteration__proxy_1_1iteration__proxy__internal.html", null ],
    [ "Joystick", "classJoystick.html", null ],
    [ "nlohmann::json_pointer< BasicJsonType >", "classnlohmann_1_1json__pointer.html", null ],
    [ "nlohmann::detail::json_ref< BasicJsonType >", "classnlohmann_1_1detail_1_1json__ref.html", null ],
    [ "nlohmann::json_sax< BasicJsonType >", "structnlohmann_1_1json__sax.html", null ],
    [ "nlohmann::detail::json_sax_acceptor< BasicJsonType >", "classnlohmann_1_1detail_1_1json__sax__acceptor.html", null ],
    [ "nlohmann::detail::json_sax_dom_callback_parser< BasicJsonType >", "classnlohmann_1_1detail_1_1json__sax__dom__callback__parser.html", null ],
    [ "nlohmann::detail::json_sax_dom_parser< BasicJsonType >", "classnlohmann_1_1detail_1_1json__sax__dom__parser.html", null ],
    [ "nlohmann::basic_json< ObjectType, ArrayType, StringType, BooleanType, NumberIntegerType, NumberUnsignedType, NumberFloatType, AllocatorType, JSONSerializer >::json_value", "unionnlohmann_1_1basic__json_1_1json__value.html", null ],
    [ "Koala_main", "classKoala__main.html", null ],
    [ "LedMatrix", "classLedMatrix.html", null ],
    [ "std::less< ::nlohmann::detail::value_t >", "structstd_1_1less_3_01_1_1nlohmann_1_1detail_1_1value__t_01_4.html", null ],
    [ "nlohmann::detail::lexer< BasicJsonType >", "classnlohmann_1_1detail_1_1lexer.html", null ],
    [ "Matrix", "classMatrix.html", null ],
    [ "nlohmann::detail::merge_and_renumber< Sequence1, Sequence2 >", "structnlohmann_1_1detail_1_1merge__and__renumber.html", null ],
    [ "nlohmann::detail::merge_and_renumber< make_index_sequence< N/2 >::type, make_index_sequence< N - N/2 >::type >", "structnlohmann_1_1detail_1_1merge__and__renumber.html", [
      [ "nlohmann::detail::make_index_sequence< N >", "structnlohmann_1_1detail_1_1make__index__sequence.html", null ]
    ] ],
    [ "mosquittopp", null, [
      [ "CommandProcessor", "classCommandProcessor.html", null ],
      [ "MQTT", "classMQTT.html", null ]
    ] ],
    [ "MQTTmessage", "classMQTTmessage.html", null ],
    [ "Node", "classNode.html", null ],
    [ "nlohmann::detail::nonesuch", "structnlohmann_1_1detail_1_1nonesuch.html", null ],
    [ "nlohmann::detail::output_adapter< CharType, StringType >", "classnlohmann_1_1detail_1_1output__adapter.html", null ],
    [ "nlohmann::detail::output_adapter_protocol< CharType >", "structnlohmann_1_1detail_1_1output__adapter__protocol.html", [
      [ "nlohmann::detail::output_stream_adapter< CharType >", "classnlohmann_1_1detail_1_1output__stream__adapter.html", null ],
      [ "nlohmann::detail::output_string_adapter< CharType, StringType >", "classnlohmann_1_1detail_1_1output__string__adapter.html", null ],
      [ "nlohmann::detail::output_vector_adapter< CharType >", "classnlohmann_1_1detail_1_1output__vector__adapter.html", null ]
    ] ],
    [ "ParExe", "classParExe.html", null ],
    [ "ParLoop", "classParLoop.html", null ],
    [ "ParQueue< D >", "classParQueue.html", null ],
    [ "ParQueue< topic_message_t >", "classParQueue.html", null ],
    [ "nlohmann::detail::parser< BasicJsonType >", "classnlohmann_1_1detail_1_1parser.html", null ],
    [ "ParWait", "classParWait.html", null ],
    [ "Pixel", "classPixel.html", null ],
    [ "nlohmann::detail::primitive_iterator_t", "classnlohmann_1_1detail_1_1primitive__iterator__t.html", null ],
    [ "nlohmann::detail::priority_tag< N >", "structnlohmann_1_1detail_1_1priority__tag.html", null ],
    [ "nlohmann::detail::priority_tag< 0 >", "structnlohmann_1_1detail_1_1priority__tag_3_010_01_4.html", null ],
    [ "reverse_iterator", null, [
      [ "nlohmann::detail::json_reverse_iterator< Base >", "classnlohmann_1_1detail_1_1json__reverse__iterator.html", null ]
    ] ],
    [ "SenseHAT", "classSenseHAT.html", [
      [ "SenseHAT_Koala", "classSenseHAT__Koala.html", null ]
    ] ],
    [ "SensorHandler", "classSensorHandler.html", null ],
    [ "nlohmann::detail::serializer< BasicJsonType >", "classnlohmann_1_1detail_1_1serializer.html", null ],
    [ "SerialLink", "classSerialLink.html", null ],
    [ "SH_ui", "classSH__ui.html", null ],
    [ "nlohmann::detail::static_const< T >", "structnlohmann_1_1detail_1_1static__const.html", null ],
    [ "TheThingsNetwork_HANIoT", "classTheThingsNetwork__HANIoT.html", null ],
    [ "nlohmann::detail::to_json_fn", "structnlohmann_1_1detail_1_1to__json__fn.html", null ],
    [ "Topic", "classTopic.html", null ],
    [ "true_type", null, [
      [ "nlohmann::detail::conjunction< is_complete_type< CompatibleType >, is_compatible_complete_type< BasicJsonType, CompatibleType > >", "structnlohmann_1_1detail_1_1conjunction.html", [
        [ "nlohmann::detail::is_compatible_type< BasicJsonType, CompatibleType >", "structnlohmann_1_1detail_1_1is__compatible__type.html", null ]
      ] ],
      [ "nlohmann::detail::conjunction<... >", "structnlohmann_1_1detail_1_1conjunction.html", null ],
      [ "nlohmann::detail::is_basic_json< NLOHMANN_BASIC_JSON_TPL >", "structnlohmann_1_1detail_1_1is__basic__json_3_01NLOHMANN__BASIC__JSON__TPL_01_4.html", null ],
      [ "nlohmann::detail::is_complete_type< T, decltype(void(sizeof(T)))>", "structnlohmann_1_1detail_1_1is__complete__type_3_01T_00_01decltype_07void_07sizeof_07T_08_08_08_4.html", null ]
    ] ],
    [ "type", null, [
      [ "nlohmann::detail::conjunction< B1, Bn... >", "structnlohmann_1_1detail_1_1conjunction_3_01B1_00_01Bn_8_8_8_01_4.html", null ]
    ] ],
    [ "waveform", "classwaveform.html", [
      [ "cosine", "classcosine.html", null ],
      [ "rectangle", "classrectangle.html", null ],
      [ "sawtooth", "classsawtooth.html", null ],
      [ "sine", "classsine.html", null ],
      [ "triangle", "classtriangle.html", null ]
    ] ]
];