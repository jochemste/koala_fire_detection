var searchData=
[
  ['rbegin',['rbegin',['../classnlohmann_1_1basic__json.html#a1ef93e2006dbe52667294f5ef38b0b10',1,'nlohmann::basic_json::rbegin() noexcept'],['../classnlohmann_1_1basic__json.html#a515e7618392317dbf4b72d3e18bf2ab2',1,'nlohmann::basic_json::rbegin() const noexcept']]],
  ['read',['read',['../classSerialLink.html#a72e3bc4cbecf4909a9df216f4393075a',1,'SerialLink']]],
  ['read_5fint',['read_int',['../classFile.html#afedbc8765f38a3e5ef6e07b33a599dad',1,'File']]],
  ['read_5fmessage',['read_message',['../classKoala__main.html#a218d934567e143da5bff3be7b48b4a08',1,'Koala_main']]],
  ['removecomment',['removeComment',['../classINIreader.html#a8512b59da07ad3143169639dd3c18057',1,'INIreader']]],
  ['rend',['rend',['../classnlohmann_1_1basic__json.html#ac77aed0925d447744676725ab0b6d535',1,'nlohmann::basic_json::rend() noexcept'],['../classnlohmann_1_1basic__json.html#a4f73d4cee67ea328d785979c22af0ae1',1,'nlohmann::basic_json::rend() const noexcept']]],
  ['replace_5fsubstring',['replace_substring',['../classnlohmann_1_1json__pointer.html#aa7649d30da9fc10b0e20704a27aea2a9',1,'nlohmann::json_pointer']]],
  ['reset',['reset',['../classnlohmann_1_1detail_1_1lexer.html#acba34bc18af19f93186e682d02c3942d',1,'nlohmann::detail::lexer']]],
  ['riskcalculation',['RiskCalculation',['../classSensorHandler.html#a34e843d17278536b1053c95b12ae3598',1,'SensorHandler']]],
  ['run',['run',['../classKoala__main.html#afaf0915fc98fb2b59de829842e863b30',1,'Koala_main']]]
];
