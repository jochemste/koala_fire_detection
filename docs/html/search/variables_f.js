var searchData=
[
  ['ref_5fstack',['ref_stack',['../classnlohmann_1_1detail_1_1json__sax__dom__parser.html#aa7526e7ae7be7f63803a23fd8cf36e5d',1,'nlohmann::detail::json_sax_dom_parser::ref_stack()'],['../classnlohmann_1_1detail_1_1json__sax__dom__callback__parser.html#a82a0e49479660b9903597e1ef55c6601',1,'nlohmann::detail::json_sax_dom_callback_parser::ref_stack()']]],
  ['reference_5ftokens',['reference_tokens',['../classnlohmann_1_1json__pointer.html#a07a990a6838de4f38ee9d881e7b9fd61',1,'nlohmann::json_pointer']]],
  ['rgb_5fhum_5foffset_5f',['rgb_hum_offset_',['../classNode.html#a374bf76db7b30bade477bef157b5c4d6',1,'Node']]],
  ['rgb_5foffset_5f',['rgb_offset_',['../classNode.html#adbf7a4230ee469e6a817237651559a82',1,'Node']]],
  ['root',['root',['../classnlohmann_1_1detail_1_1json__sax__dom__parser.html#aef0477277389e399d7128898841b71c0',1,'nlohmann::detail::json_sax_dom_parser::root()'],['../classnlohmann_1_1detail_1_1json__sax__dom__callback__parser.html#a7b12037ca38be3ddec09c42bd71080cc',1,'nlohmann::detail::json_sax_dom_callback_parser::root()']]],
  ['rowcoords_5f',['rowCoords_',['../classMatrix.html#a407d5f6cf059139015aea19ff375714f',1,'Matrix']]],
  ['rows_5f',['rows_',['../classSenseHAT__Koala.html#afcbf30a0b38a8c7edfc2dab2b39ce6ad',1,'SenseHAT_Koala']]]
];
