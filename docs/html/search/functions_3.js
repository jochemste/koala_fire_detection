var searchData=
[
  ['decode',['decode',['../classnlohmann_1_1detail_1_1serializer.html#a1cc5cb2f6993cefedad087a6cef0e8fb',1,'nlohmann::detail::serializer']]],
  ['diff',['diff',['../classnlohmann_1_1basic__json.html#a543bd5f7490de54c875b2c0912dc9a49',1,'nlohmann::basic_json']]],
  ['draw',['draw',['../classSenseHAT__Koala.html#a8099a1c9681e6bb189a024c1aa3d6579',1,'SenseHAT_Koala::draw(int column, int row, int r, int g, int b)'],['../classSenseHAT__Koala.html#a5eac343dd4a94817c9e105b4060e2f1e',1,'SenseHAT_Koala::draw(int column, int row, Pixel &amp;pixel)']]],
  ['draw_5fnodes_5fhum',['draw_nodes_hum',['../classSH__ui.html#acbb88e1539f12f0b23554f45be3c2069',1,'SH_ui']]],
  ['draw_5fnodes_5frisk',['draw_nodes_risk',['../classSH__ui.html#a6e8ddfed9e3b5f09e65ceeb6958cbbbb',1,'SH_ui']]],
  ['draw_5fnodes_5ftemp',['draw_nodes_temp',['../classSH__ui.html#a37f2c9da76805e16c2212e4a71d33a82',1,'SH_ui']]],
  ['drawend',['drawEnd',['../classSenseHAT__Koala.html#a431d765b72cecc5b1e9d829021118128',1,'SenseHAT_Koala']]],
  ['drawh',['drawH',['../classSenseHAT__Koala.html#ad5ab83137c6fb08536f19d68818d6a16',1,'SenseHAT_Koala']]],
  ['drawr',['drawR',['../classSenseHAT__Koala.html#a5718a103b67b1337863f7e661c1342ad',1,'SenseHAT_Koala']]],
  ['drawt',['drawT',['../classSenseHAT__Koala.html#a529a58e47a6a86f9ed98d330a46fc676',1,'SenseHAT_Koala']]],
  ['drawtest',['drawTest',['../classSenseHAT__Koala.html#aa45ef6ca53c2b222fa53786deae0f784',1,'SenseHAT_Koala']]],
  ['dump',['dump',['../classnlohmann_1_1detail_1_1serializer.html#a95460ebd1a535a543e5a0ec52e00f48b',1,'nlohmann::detail::serializer::dump()'],['../classnlohmann_1_1basic__json.html#a5adea76fedba9898d404fef8598aa663',1,'nlohmann::basic_json::dump()']]],
  ['dump_5fescaped',['dump_escaped',['../classnlohmann_1_1detail_1_1serializer.html#ac1f8d1165b44149bd8be397dce68ea05',1,'nlohmann::detail::serializer']]],
  ['dump_5ffloat',['dump_float',['../classnlohmann_1_1detail_1_1serializer.html#a6d652a3bfa581cf1cd7790d6d11ea52f',1,'nlohmann::detail::serializer']]],
  ['dump_5finteger',['dump_integer',['../classnlohmann_1_1detail_1_1serializer.html#a944f6dea8dbe2961da145d2f62fa2c2f',1,'nlohmann::detail::serializer']]]
];
