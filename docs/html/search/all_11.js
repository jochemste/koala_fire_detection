var searchData=
[
  ['rbegin',['rbegin',['../classnlohmann_1_1basic__json.html#a1ef93e2006dbe52667294f5ef38b0b10',1,'nlohmann::basic_json::rbegin() noexcept'],['../classnlohmann_1_1basic__json.html#a515e7618392317dbf4b72d3e18bf2ab2',1,'nlohmann::basic_json::rbegin() const noexcept']]],
  ['read',['read',['../classSerialLink.html#a72e3bc4cbecf4909a9df216f4393075a',1,'SerialLink']]],
  ['read_5fint',['read_int',['../classFile.html#afedbc8765f38a3e5ef6e07b33a599dad',1,'File']]],
  ['read_5fmessage',['read_message',['../classKoala__main.html#a218d934567e143da5bff3be7b48b4a08',1,'Koala_main']]],
  ['rectangle',['rectangle',['../classrectangle.html',1,'']]],
  ['ref_5fstack',['ref_stack',['../classnlohmann_1_1detail_1_1json__sax__dom__parser.html#aa7526e7ae7be7f63803a23fd8cf36e5d',1,'nlohmann::detail::json_sax_dom_parser::ref_stack()'],['../classnlohmann_1_1detail_1_1json__sax__dom__callback__parser.html#a82a0e49479660b9903597e1ef55c6601',1,'nlohmann::detail::json_sax_dom_callback_parser::ref_stack()']]],
  ['reference',['reference',['../classnlohmann_1_1detail_1_1iter__impl.html#a5be8001be099c6b82310f4d387b953ce',1,'nlohmann::detail::iter_impl::reference()'],['../classnlohmann_1_1detail_1_1json__reverse__iterator.html#a42f51a69bac7b2aebb613b2164e457f1',1,'nlohmann::detail::json_reverse_iterator::reference()'],['../classnlohmann_1_1basic__json.html#ac6a5eddd156c776ac75ff54cfe54a5bc',1,'nlohmann::basic_json::reference()']]],
  ['reference_5ftokens',['reference_tokens',['../classnlohmann_1_1json__pointer.html#a07a990a6838de4f38ee9d881e7b9fd61',1,'nlohmann::json_pointer']]],
  ['released',['RELEASED',['../HAN__IoT__Shield_8h.html#ad74b7f5218b46c8332cd531df7178d45',1,'HAN_IoT_Shield.h']]],
  ['removecomment',['removeComment',['../classINIreader.html#a8512b59da07ad3143169639dd3c18057',1,'INIreader']]],
  ['rend',['rend',['../classnlohmann_1_1basic__json.html#ac77aed0925d447744676725ab0b6d535',1,'nlohmann::basic_json::rend() noexcept'],['../classnlohmann_1_1basic__json.html#a4f73d4cee67ea328d785979c22af0ae1',1,'nlohmann::basic_json::rend() const noexcept']]],
  ['replace_5fsubstring',['replace_substring',['../classnlohmann_1_1json__pointer.html#aa7649d30da9fc10b0e20704a27aea2a9',1,'nlohmann::json_pointer']]],
  ['reset',['reset',['../classnlohmann_1_1detail_1_1lexer.html#acba34bc18af19f93186e682d02c3942d',1,'nlohmann::detail::lexer']]],
  ['reverse_5fiterator',['reverse_iterator',['../classnlohmann_1_1basic__json.html#ac223d5560c2b05a208c88de67376c5f2',1,'nlohmann::basic_json']]],
  ['rgb_5fhum_5foffset_5f',['rgb_hum_offset_',['../classNode.html#a374bf76db7b30bade477bef157b5c4d6',1,'Node']]],
  ['rgb_5foffset_5f',['rgb_offset_',['../classNode.html#adbf7a4230ee469e6a817237651559a82',1,'Node']]],
  ['riskcalculation',['RiskCalculation',['../classSensorHandler.html#a34e843d17278536b1053c95b12ae3598',1,'SensorHandler']]],
  ['root',['root',['../classnlohmann_1_1detail_1_1json__sax__dom__parser.html#aef0477277389e399d7128898841b71c0',1,'nlohmann::detail::json_sax_dom_parser::root()'],['../classnlohmann_1_1detail_1_1json__sax__dom__callback__parser.html#a7b12037ca38be3ddec09c42bd71080cc',1,'nlohmann::detail::json_sax_dom_callback_parser::root()']]],
  ['rowcoords_5f',['rowCoords_',['../classMatrix.html#a407d5f6cf059139015aea19ff375714f',1,'Matrix']]],
  ['rows_5f',['rows_',['../classSenseHAT__Koala.html#afcbf30a0b38a8c7edfc2dab2b39ce6ad',1,'SenseHAT_Koala']]],
  ['run',['run',['../classKoala__main.html#afaf0915fc98fb2b59de829842e863b30',1,'Koala_main']]]
];
