var searchData=
[
  ['next_5funget',['next_unget',['../classnlohmann_1_1detail_1_1lexer.html#ae8bedb97b907ba6347c5b2f2666ca01f',1,'nlohmann::detail::lexer']]],
  ['nodes_5f',['nodes_',['../classSH__ui.html#a5dc185aa5e5f295acb31809f208e1560',1,'SH_ui']]],
  ['nrcolumns_5f',['nrColumns_',['../classMatrix.html#acbd143db4422f027d8acb67e70d597ea',1,'Matrix']]],
  ['nrrows_5f',['nrRows_',['../classMatrix.html#aa28e9644221aa86762fb9ac052ba3be1',1,'Matrix']]],
  ['number_5fbuffer',['number_buffer',['../classnlohmann_1_1detail_1_1serializer.html#a1a9d8b344a6cb47728a3519693ec03d1',1,'nlohmann::detail::serializer']]],
  ['number_5ffloat',['number_float',['../unionnlohmann_1_1basic__json_1_1json__value.html#ad003495e39e78b8096e0b6fc690d146f',1,'nlohmann::basic_json::json_value']]],
  ['number_5finteger',['number_integer',['../unionnlohmann_1_1basic__json_1_1json__value.html#afa3c414445aeffb56a7c6926f9420941',1,'nlohmann::basic_json::json_value']]],
  ['number_5funsigned',['number_unsigned',['../unionnlohmann_1_1basic__json_1_1json__value.html#a0299a6aa3bc4d45d54130e52970f73d3',1,'nlohmann::basic_json::json_value']]]
];
