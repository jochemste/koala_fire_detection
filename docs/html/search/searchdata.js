var indexSectionsWithContent =
{
  0: "_abcdefghijklmnoprstuvwy~",
  1: "abcdefhijklmnoprstw",
  2: "anu",
  3: "ht",
  4: "abcdefghijklmnoprstuvw~",
  5: "_abcdefhiklmnoprstuy",
  6: "abcdeijnoprstv",
  7: "bilptv",
  8: "abdeklnopsuv",
  9: "o",
  10: "pr",
  11: "dkt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "related",
  10: "defines",
  11: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Friends",
  10: "Macros",
  11: "Pages"
};

