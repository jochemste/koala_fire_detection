var classSenseHAT =
[
    [ "SenseHAT", "classSenseHAT.html#a6f1b370e30d524ee6bccaa19c5566fbe", null ],
    [ "SenseHAT", "classSenseHAT.html#a39f7095d97dd434479c05e85bdc32952", null ],
    [ "~SenseHAT", "classSenseHAT.html#a1af79051ee9d294203278d8d3f22036a", null ],
    [ "operator=", "classSenseHAT.html#aa05efe7c4e0933067e3b655470dfd6df", null ],
    [ "get_temperature", "classSenseHAT.html#ae0d2f1c110a5dfe453bdb2f22cf719c0", null ],
    [ "get_humidity", "classSenseHAT.html#a270dbefc2b736c1ef3dfc996f968611f", null ],
    [ "get_temperature_from_humidity", "classSenseHAT.html#af1ffda111c9826f7eb3b88aa0ea29a00", null ],
    [ "get_pressure", "classSenseHAT.html#a6fcb3739537d351a60375a8aa8309a4e", null ],
    [ "get_temperature_from_pressure", "classSenseHAT.html#abe8f9a29726dbfeac2bd45cff6f8a984", null ],
    [ "stick", "classSenseHAT.html#a69d5712bf0663364109f9a482178dc2c", null ],
    [ "leds", "classSenseHAT.html#a09557e42b20ddddc1763917bd36f36a7", null ],
    [ "humidity", "classSenseHAT.html#ac0af7c3bda280b9d60257c3860b54a5a", null ],
    [ "pressure", "classSenseHAT.html#a4c589c21bc7351013891296114f27421", null ]
];