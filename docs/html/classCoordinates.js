var classCoordinates =
[
    [ "Coordinates", "classCoordinates.html#af98b3cdf06c918fb13e758b20bf96900", null ],
    [ "Coordinates", "classCoordinates.html#a2b4a01428b4aa69658f1fb4d540aab13", null ],
    [ "~Coordinates", "classCoordinates.html#add7d735f38950ba96dcc3f9ebfac7870", null ],
    [ "operator==", "classCoordinates.html#a3d91db2a0d8643f2cc410652594691d6", null ],
    [ "operator+", "classCoordinates.html#ae3023d0b85c714d424397419af3e5d2e", null ],
    [ "operator-", "classCoordinates.html#a1a50c64e356e5c71245436f0ffd1cc1c", null ],
    [ "operator*", "classCoordinates.html#a726e6a667a7958c3aa9b20a1d639a1c7", null ],
    [ "operator/", "classCoordinates.html#a5f1503e56eb89efd1e1f35b2b61911b3", null ],
    [ "operator=", "classCoordinates.html#a69f77696d70f40693fb6a15c7b7833de", null ],
    [ "operator+=", "classCoordinates.html#a1cb590ea0170e486005033d0ed2db558", null ],
    [ "operator-=", "classCoordinates.html#adbe709d2ce6d40b82de97a7dfebdd025", null ],
    [ "setX", "classCoordinates.html#aca71feab3bae10323fb21863e7eabfd1", null ],
    [ "setY", "classCoordinates.html#afd17cc63ff99fa037ec9b5e50dfeb5a3", null ],
    [ "getX", "classCoordinates.html#ad5bd5f1848df93aa21493ca966128ef3", null ],
    [ "getY", "classCoordinates.html#a4d5e072b148b0f3421499a64bcf36197", null ],
    [ "x_", "classCoordinates.html#a77d2da737b92695d15342a3a48d81bb9", null ],
    [ "y_", "classCoordinates.html#a79d30ab702c838109135f4cfc4161fca", null ]
];