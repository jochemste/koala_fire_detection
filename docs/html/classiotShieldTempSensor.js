var classiotShieldTempSensor =
[
    [ "iotShieldTempSensor", "classiotShieldTempSensor.html#a2a05ec29fe20a0b57687d883ba172fde", null ],
    [ "~iotShieldTempSensor", "classiotShieldTempSensor.html#afe5e7613058016daffe1ebb2024f0a22", null ],
    [ "getTemperatureCelsius", "classiotShieldTempSensor.html#a3ddb1e481c45898cb3a0d9696467165c", null ],
    [ "_temperature", "classiotShieldTempSensor.html#a1370bf4b49e1aeee2ea959075d3a70a7", null ],
    [ "_oneWireInterface", "classiotShieldTempSensor.html#afa98a216303a256111e38deb20d8dec7", null ],
    [ "_sensors", "classiotShieldTempSensor.html#a6ec6abba2097add91d2dc71a3945773f", null ]
];