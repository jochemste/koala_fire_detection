var classwaveform =
[
    [ "waveform", "classwaveform.html#ad04c90ee50012d19c5713912b2248069", null ],
    [ "~waveform", "classwaveform.html#ad09741657cdce4d7449ea752dbeee5ea", null ],
    [ "getSignal", "classwaveform.html#a3636df905b6df4e9f6e75130ae66c153", null ],
    [ "getStep", "classwaveform.html#aaff5cde87b15169d7ce73a139ee37225", null ],
    [ "floatMap", "classwaveform.html#a059e081f91e3b98163a262576cb1dc74", null ],
    [ "_minimumValue", "classwaveform.html#ae7c68441f39700fb6f5c982f0cbcc9a1", null ],
    [ "_maximumValue", "classwaveform.html#a0cbd21aed38ccd781019e532855ec2fb", null ],
    [ "_step", "classwaveform.html#a689a0d1f0cedfe998a5e36038edaa2c7", null ]
];