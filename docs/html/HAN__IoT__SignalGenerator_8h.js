var HAN__IoT__SignalGenerator_8h =
[
    [ "waveform", "classwaveform.html", "classwaveform" ],
    [ "sine", "classsine.html", "classsine" ],
    [ "cosine", "classcosine.html", "classcosine" ],
    [ "triangle", "classtriangle.html", "classtriangle" ],
    [ "rectangle", "classrectangle.html", "classrectangle" ],
    [ "sawtooth", "classsawtooth.html", "classsawtooth" ],
    [ "PIE", "HAN__IoT__SignalGenerator_8h.html#a8f854d0f89ac9b4c8bae03dd4720b66d", null ],
    [ "FLT_EPSILON", "HAN__IoT__SignalGenerator_8h.html#a71281bc9e7223e069b3efbbf043799c5", null ],
    [ "SMALL", "HAN__IoT__SignalGenerator_8h.html#a8e5cddd82bd07dbb401640bcedcce266", null ],
    [ "BIG", "HAN__IoT__SignalGenerator_8h.html#a75df2cffbe08f38fbca67f2e1f3b4bca", null ],
    [ "STEP_COUNT_HALF_PERIOD", "HAN__IoT__SignalGenerator_8h.html#aeeea061278bba34a5c9699d5ad490a07", null ]
];