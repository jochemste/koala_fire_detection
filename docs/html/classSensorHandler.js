var classSensorHandler =
[
    [ "~SensorHandler", "classSensorHandler.html#a6009720f91d020e25a95c74c2d4e05cc", null ],
    [ "getTemperatureCel", "classSensorHandler.html#aff5f06a564feaa4dbf008b7a3ae180c3", null ],
    [ "getHumidity", "classSensorHandler.html#a5e64012955899aafcd8269fedd844e17", null ],
    [ "RiskCalculation", "classSensorHandler.html#a34e843d17278536b1053c95b12ae3598", null ],
    [ "temperature", "classSensorHandler.html#a254ed7b4c1b4e0c93c51dda97bf51fb5", null ],
    [ "humidity", "classSensorHandler.html#a8118f88efead61c3830708b038299416", null ],
    [ "calTemperature", "classSensorHandler.html#ad2c6d3242fbe918560e7065b2f418944", null ],
    [ "constrainedTemperature", "classSensorHandler.html#ab98684034b8a98f98a9ab65570aeb4a2", null ],
    [ "calHumidity", "classSensorHandler.html#a32788d899140b06fba598464c192256d", null ],
    [ "constrainedHumidity", "classSensorHandler.html#ace70994e9ef745887a338697f76d2f8d", null ],
    [ "tm", "classSensorHandler.html#a908a6a65cb6281732a391d307e25f2d5", null ],
    [ "tm_temperature", "classSensorHandler.html#a68696716a948563e6512b7a1d958f591", null ],
    [ "tm_humidity", "classSensorHandler.html#a4da22005bce66bb16a1ac8c3060e69dd", null ],
    [ "weight_t", "classSensorHandler.html#ad94a4c09f8dfb5cb389b8a53caed7bd4", null ],
    [ "weight_h", "classSensorHandler.html#abdbd61bf50b553affd19659b3bc36b42", null ]
];