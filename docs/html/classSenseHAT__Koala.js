var classSenseHAT__Koala =
[
    [ "SenseHAT_Koala", "classSenseHAT__Koala.html#a24875dfc76b751a95e1f4ee1ef6d58da", null ],
    [ "~SenseHAT_Koala", "classSenseHAT__Koala.html#ad6ca1f0cece87ed9ea7a6d7339c4af6e", null ],
    [ "drawTest", "classSenseHAT__Koala.html#aa45ef6ca53c2b222fa53786deae0f784", null ],
    [ "drawEnd", "classSenseHAT__Koala.html#a431d765b72cecc5b1e9d829021118128", null ],
    [ "draw", "classSenseHAT__Koala.html#a8099a1c9681e6bb189a024c1aa3d6579", null ],
    [ "draw", "classSenseHAT__Koala.html#a5eac343dd4a94817c9e105b4060e2f1e", null ],
    [ "drawR", "classSenseHAT__Koala.html#a5718a103b67b1337863f7e661c1342ad", null ],
    [ "drawT", "classSenseHAT__Koala.html#a529a58e47a6a86f9ed98d330a46fc676", null ],
    [ "drawH", "classSenseHAT__Koala.html#ad5ab83137c6fb08536f19d68818d6a16", null ],
    [ "toggle", "classSenseHAT__Koala.html#ad128914420a5c5820d96cad05334774f", null ],
    [ "toggle", "classSenseHAT__Koala.html#ab40ad963ebb0f6cdbf957c02147b5281", null ],
    [ "clear", "classSenseHAT__Koala.html#ad2b1eafa79959685b8ced4e24a75ce89", null ],
    [ "clear", "classSenseHAT__Koala.html#ad2abcf9517100e0fe68942d8d53c8cb8", null ],
    [ "clear", "classSenseHAT__Koala.html#af71fe294d625720c9dd814a8597a0dff", null ],
    [ "background_", "classSenseHAT__Koala.html#a5fd83bf0366ce526a71dd26a3b31cf99", null ],
    [ "letters_", "classSenseHAT__Koala.html#aa37343c7dd2dad34d2e83934a662541d", null ],
    [ "columns_", "classSenseHAT__Koala.html#a9f242b9b67e38e28c64178553a9a58cf", null ],
    [ "rows_", "classSenseHAT__Koala.html#afcbf30a0b38a8c7edfc2dab2b39ce6ad", null ]
];