var NAVTREE =
[
  [ "Koala fire detection", "index.html", [
    [ "Koala Forest Fire Detection System", "index.html", [
      [ "Introduction", "index.html#Introduction", null ],
      [ "Node", "index.html#Node", null ],
      [ "Koala_Pi", "index.html#Koala_Pi", null ],
      [ "UML_diagrams", "index.html#UML_diagrams", [
        [ "Sequence_diagram", "index.html#Sequence_diagram", null ],
        [ "Deployment_diagram", "index.html#Deployment_diagram", null ],
        [ "Node_Classdiagram", "index.html#Node_Classdiagram", null ],
        [ "Koala_Pi_Classdiagram", "index.html#Koala_Pi_Classdiagram", null ]
      ] ],
      [ "Calculations", "index.html#Calculations", [
        [ "Transmission_interval_calculations", "index.html#Transmission_interval_calculations", null ],
        [ "RGB_colour_calculations", "index.html#RGB_colour_calculations", null ],
        [ "Power_usage_calculations", "index.html#Power_usage_calculations", null ]
      ] ],
      [ "Research", "index.html#Research", [
        [ "Background", "index.html#Background", null ],
        [ "Type_of_sensors", "index.html#Type_of_sensors", null ],
        [ "Smoke_sensors", "index.html#Smoke_sensors", null ],
        [ "Optical_Flame_Detectors", "index.html#Optical_Flame_Detectors", null ],
        [ "Wildfire_detection", "index.html#Wildfire_detection", null ],
        [ "Concept_1", "index.html#Concept_1", null ],
        [ "Concept_2", "index.html#Concept_2", null ],
        [ "Sensor_selection", "index.html#Sensor_selection", null ]
      ] ]
    ] ],
    [ "Todo List", "todo.html", null ],
    [ "Deprecated List", "deprecated.html", null ],
    [ "Namespaces", null, [
      [ "Namespace List", "namespaces.html", "namespaces" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Variables", "globals_vars.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
".html",
"classCoordinates.html#a5f1503e56eb89efd1e1f35b2b61911b3",
"classParLoop.html#ac639a551688711ac8436140c92c949a3",
"classiotShieldLED.html#a86a47fe384f63e7b81b43298e835e12d",
"classnlohmann_1_1basic__json.html#afff7860310ae69f29f8158a77ec0ef13",
"classnlohmann_1_1detail_1_1json__sax__dom__parser.html#ad0953e8d888339421d909d9016bc6e2c",
"functions_enum.html",
"structnlohmann_1_1json__sax.html#a2e0c7ecd80b18d18a8cc76f71cfc2028"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';