/*
 * This is the sensor handler for the kowala smart forest firealarm.
 * the sensors will be actived by this file.
 * 
 * version 1.2
 * -v1.1 changed teamperature read to potmeter read
 * -v1.2 added the risk calculation
 * -v1.3 changed the risk calculation to 24hours instead of 6 + added doxygen comments
 */

#include "SensorHandler.h"

// Configure HAN IoT Shield
// =========================
iotShieldPotmeter potmeter1(PIN_POT_RED, -20, 500);
iotShieldPotmeter potmeter2(PIN_POT_WHITE, 0, 100);

//IOT SHIELD CONFIG
iotShieldTempSensor temperatureSensor;

SensorHandler::~SensorHandler(){}

float SensorHandler::getTemperatureCel(){

  temperature = potmeter1.getValue();
  return temperature;
}

float SensorHandler::getHumidity(){

  humidity = potmeter2.getValue();
  return limitHumidity;
}

float SensorHandler::RiskCalculation ()
{
  calTemperature = getTemperatureCel();
  calHumidity = getHumidity();
  
  constrainedTemperature = constrain( calTemperature, 0, 30);      //limits the temperature to 0 - 30 *c for the calcuation.
  constrainedHumidity = constrain( calHumidity, 20, 80);           //limits humidity to 20% - 80% for the calculation
  
  tm_temperature = (-0.7997222*constrainedTemperature+24)*3600;     //Calculate transmit time in seconds for temperature
  tm_humidity = (0.398527*(constrainedHumidity-20) )*3600+30;    //Calculate transmit time in seconds for humidity
  Serial.print("Tm_t"+String(tm_temperature));
  Serial.println();
  Serial.print("Tm_h"+String(tm_humidity));
  Serial.println();

  weight_t = 1-(tm_temperature/(tm_temperature+tm_humidity));   //Calculate weight of the transmit time. The shorter the time, the higher the weight
  weight_h = 1-(tm_humidity/(tm_temperature+tm_humidity));      //Calculate weight of the transmit time. The shorter the time, the higher the weight

  tm = weight_t*tm_temperature + weight_h *tm_humidity;         //Final transmit time to used by node and rpi
  return tm;
}
