/*
 * This is the sensor handler for the kowala smart forest firealarm.
 * the sensors will be actived by this file.
 * 
  * version 1.2
 * -v1.1 changed teamperature read to potmeter read
 * -v1.2 added the risk calculation
 * -v1.3 changed risk calculation to 24 hours instead of 6+ added doxygen comments
 */

#ifndef SENSORHANDLER_H
#define SENSORHANDLER_H

//#include <arduino.h>
#include "HAN_IoT_Shield.h"

class SensorHandler
 {
  private:
  
  float temperature;//temperature of the potmeter
  float humidity;//humidity of the potmeter
        
  float calTemperature;// neccesary to limit the input value of temperature for the risk calculation.
  float constrainedTemperature;// neccesary to limit the input value of temperature for the risk calculation.
  float calHumidity;// neccesary to limit the input value of humidity for the risk calculation.
  float constrainedHumidity;// neccesary to limit the input value of humidity for the risk calculation.
  
  float tm;//transmission time
  float tm_temperature;//transmission time from temperature
  float tm_humidity;//transmission time from humidity
  float weight_t;//weight of temperatuer in calculation
  float weight_h;//weight of humidity in calculation
  
  public:

    /*!
   * \brief get Temperature in Celcius
   * \details Gets the temperature in celcius emulated on potmeter1
   * \returns float temperature
   */
  float getTemperatureCel();
  
    /*!
   * \brief Get the humidity
   * \details Gets the humidity emulated on the potmeter2 between 0 to 100
   * \returns float Humidity
   */
  float getHumidity();

    /*!
   * \brief calculates transmission delay
   * \details uses getHUmidity and getTemperatureCel to calculate the transmission delay.
   * \returns float transmission delay
   */
  float RiskCalculation();

    /*!
   * \brief The Class destructor
   * \details Cleans up SensorHandler
   */
  ~SensorHandler(); //default constructor

  
 };
#endif// SensorHandler
